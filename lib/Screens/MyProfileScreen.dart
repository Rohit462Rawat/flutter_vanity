import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/model/ModelProfileResponse.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:flutter_demo/util/imagepicker/image_picker_handler.dart';
import 'package:flutter_multiple_image_picker/flutter_multiple_image_picker.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:flutter/services.dart';





class MyProfileScreen extends StatefulWidget {
  @override
  _MyProfileScreenState createState() => _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen>  with TickerProviderStateMixin,ImagePickerListener {
//  List<Asset> multiple_images = List<Asset>();  String _error = 'No Error Dectected';


  //BuildContext context;
  String _platformMessage = 'No Error';
  var images;
  int maxImageNo = 10;
  bool selectSingleImage = false;
  File _image;
  var imageType="";
  var imageListMultiple=List<String>();

  var imageMultipleApiResponse=List<String>();

  File _imageUserProfile;
  AnimationController _controller;
  ImagePickerHandler imagePicker;

  String selectedDate,editable;
  SharedPreferences prefs;
  ModelProfileData data;
  var fname = TextEditingController();
  var lname = TextEditingController();
  var email = TextEditingController();
  var dob = TextEditingController();
  var current_agency = TextEditingController();
  var mother_agency = TextEditingController();
  var hometown = TextEditingController();
  var country = TextEditingController();
  var instagram = TextEditingController();
  var mobileNumber = TextEditingController();
  var profileImage=null;


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    loadsharedPrefrenceWithApi();

    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    imagePicker=new ImagePickerHandler(this,_controller);
    imagePicker.init();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        centerTitle: true,
        title: Text("Profile"),
        backgroundColor: Colors.pink,
        actions: [
          Visibility(
            visible: editable == null ? true : false,
            child: GestureDetector(
              onTap: (){
                setState(() {
                  editable="edit";
                });
              },
              child: Container(
                  margin:EdgeInsets.fromLTRB(0, 0, 20, 0)
                  ,child: Icon(Icons.edit)),
            ),
          ),
        ],
      ),


      body: _buildUIComposer(context),
    );

  }

  Widget _buildUIComposer(BuildContext context) {
    return  data == null
        ? Center(
      child: Container(
          margin:EdgeInsets.fromLTRB(0, 0,0, 0),
          child: CircularProgressIndicator()),
    ):Container(
      height: double.infinity,
      width: double.infinity,

      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(
              left: 10, top: 20, right: 10, bottom: 20),
          child: Column(
            children: <Widget>[

             /* Container(
                width: 100.0,
                height: 100.0,
                margin: const EdgeInsets.only(left: 20, top: 0, right: 20, bottom: 20),
          decoration: new BoxDecoration(
            color: const Color(0xff7c94b6),
            image: new DecorationImage(
              image: new NetworkImage(MyConstants.USERS_IMAGE+data.profilePic),
              fit: BoxFit.cover,
            ),
            borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
            border: new Border.all(
              color: Colors.pink,
              width: 4.0,
            ),
          ),

                 *//* child: CircleAvatar(
                    //child: Image.asset('assets/images/app_icon.png'),
                    radius: 40.0,
                    backgroundImage: NetworkImage(""),
                    backgroundColor: Colors.grey,
                  ),*//*
              )*/

              GestureDetector(

                child: new Center(
                  child: Stack(
                    children: <Widget>[ Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                      height: 100.0,
                      width: 100.0,
                      child: _image == null
                          ? new Stack(
                        children: <Widget>[
                          new Center(
                            child: new CircleAvatar(
                              radius: 80.0,
                              backgroundColor: const Color(0xFF778899),
                              backgroundImage:NetworkImage(MyConstants.USERS_IMAGE+profileImage),
                            ),
                          ),
                          /* new Center(
                            child: Container(
                              height: 40,
                              child: Image.network()),
                          ),*/
                        ],
                      )
                          : new Container(
                        decoration: new BoxDecoration(
                          color: const Color(0xff7c94b6),
                          image: new DecorationImage(
                            image: new MemoryImage(loadData()),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(color: Colors.red, width: 2.0),
                          borderRadius:
                          new BorderRadius.all(const Radius.circular(80.0)),
                        ),
                      ),
                    ),
                      GestureDetector(
                        onTap: () =>
                        { imageType="profile_pic",
                          imagePicker.showDialog(context)
                        },
                        child: Visibility(
                          visible: editable == null ? false : true,
                          child: Container(width: 30,
                              height: 30,
                              margin: EdgeInsets.fromLTRB(70,70,30,0),
                              child:
                           Image.asset('assets/images/camera_icon.png')),
                        ),
                      )
                    ],

                  ),
                ),
              )
              ,

              Card(
                elevation: 3,
                margin: EdgeInsets.all(10),
                child: Container(
                  margin: EdgeInsets.all(15),
                  child: Column(children: <Widget>[
                    Row(
                        children: <Widget>[
                          Expanded(child: textFieldBackgroundUI2(
                              Text("First Name",textAlign: TextAlign.start
                                ,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold),),
                              TextField( onChanged: (text) {
                                //email = text;
                              },
                                  keyboardType: TextInputType.text,
                                  autofocus: false,
                                  enabled: editable==null?false:true,
                                  controller: fname,
                                  textInputAction: TextInputAction.done,
                                  decoration: InputDecoration(
                                    //border: InputBorder.none,
                                      hintText: "First Name",

                                      hintStyle: TextStyle(fontSize: 12)
                                  )
                                //flexible

                              )),),
                          SizedBox(width: 15,),
                          Expanded(child: textFieldBackgroundUI2(
                              Text("Last Name",
                                  textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                              TextField( onChanged: (text) {
                                //email = text;
                              },
                                  keyboardType: TextInputType.text,
                                  controller: lname,
                                  enabled: editable==null?false:true,
                                  textInputAction: TextInputAction.done,
                                  decoration: InputDecoration(
                                    //border: InputBorder.none,
                                      hintText: 'Last Name',
                                      hintStyle: TextStyle(fontSize: 12)
                                  )
                                //flexible

                              )),)
                        ]),

                    textFieldBackgroundUI2(
                        Text("Email",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            controller: email,
                            enabled: false,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                                hintText: 'Email',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),
                    textFieldBackgroundUI2(
                        Text("Dob",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        InkWell(
                          onTap: () {
                            _selectDate(context);   // Call Function that has showDatePicker()
                          },
                          child: IgnorePointer(
                            child: TextField( onChanged: (text) {
                              //email = text;
                            },
                                keyboardType: TextInputType.text,
                                controller: dob,
                                enabled: editable==null?false:true,
                                textInputAction: TextInputAction.done,
                                decoration: InputDecoration(
                                  //border: InputBorder.none,
                                    hintText: 'Dob',
                                    hintStyle: TextStyle(fontSize: 14)
                                )
                              //flexible

                            ),
                          ),
                        ))
                    , textFieldBackgroundUI2(
                        Text("Current Agency",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.text,
                            controller: current_agency,
                            enabled: editable==null?false:true,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                                hintText: 'Current Agency',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        ))
                    , textFieldBackgroundUI2(
                        Text("Mother Agency",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.text,
                            controller: mother_agency,
                            enabled: editable==null?false:true,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                                hintText: 'Mother Agency',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        ))
                    , textFieldBackgroundUI2(
                        Text("Home Town",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.text,
                            controller: hometown,
                            enabled: editable==null?false:true,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                                hintText: 'Home Town',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        ))
                    , textFieldBackgroundUI2(
                        Text("Country",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.text,
                            controller: country,
                            enabled: editable==null?false:true,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                                hintText: 'Country',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        ))
                    , textFieldBackgroundUI2(
                        Text("Instragram",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.text,
                            controller: instagram,
                            enabled: editable==null?false:true,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                                hintText: 'Instragram',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        ))
                    , textFieldBackgroundUI2(
                        Text("Mobile Number",
                            textAlign: TextAlign.start,style: TextStyle(color: Colors.pink,fontWeight: FontWeight.bold)),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.number,
                            controller: mobileNumber,
                            enabled: editable==null?false:true,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                                hintText: 'Mobile Number',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),
                    Visibility(
                      visible: editable == null ? false : true,
                      child: GestureDetector(
                       onTap: (){
                          print("dd");
                          imageType="multiple_images";
                          initMultiPickUp();
                        },
                        child: textFieldBackgroundUI1(Icon(Icons.file_upload,color: Colors.pink,),
                            Text("Upload Newest Polaroids",style: TextStyle(color: Colors.pink),)),
                      ),
                    ),

                   showingListOfMultiSelectedImages(context)
                   // buildGridView()

                  ]),
                )
              ),


              Visibility(
                  visible: editable == null ? false : true,
                  child:
                  GestureDetector(
                    onTap:(){
                      updateProfileBtn(context);
                    } ,
                    child: Container(
                    height: 50,
                    width: double.infinity,
                    margin: const EdgeInsets.only(left: 10,right: 10,
                        top: 30,  bottom: 20),
                    // padding: const EdgeInsets.all(3.0),

                    decoration: new BoxDecoration(
                      color: Colors.pink,
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(30.0),
                      ),
                    ),
                    child: FlatButton(
                       // onPressed: updateProfileBtn,
                        child: Text("Save",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontStyle: FontStyle.normal)))),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  showingListOfMultiSelectedImages(BuildContext context){
    print(data.polaroids);
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 100,
            child: /*images == null
                ? */ ListView.builder(

              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) =>
              new Padding(
                padding: const EdgeInsets.all(5.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 60,
                      child: new Image.network(
                          MyConstants.USERS_IMAGE+imageMultipleApiResponse[index].toString()),
                    ),
                    Visibility(
                      visible: editable == null ? false : true,
                      child: GestureDetector(
                        onTap: (){
                          removeImage(index);
                        },
                        child: Container(
                            width: 20,
                            height: 20,
                            margin: EdgeInsets.fromLTRB(35,0,30,5),
                            child: Image.asset('assets/images/cross_icon.png')),
                      ),
                    ),

                  ],

                ),
              ),
              itemCount: imageMultipleApiResponse.length,)
               /* ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) =>
                new Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: new Image.file(
                    File(images[index].toString()),),
                ),
                itemCount: images.length,
              ),*/
          ),

         /* new Padding(
            padding: const EdgeInsets.all(8.0),
            child: new Text('Error Dectected: $_platformMessage'),
          ),*/
         /* new RaisedButton.icon(
              onPressed: initMultiPickUp,
              icon: new Icon(Icons.image),
              label: new Text("Pick-Up Images")),*/
        ],
      ),
    );
  }

  removeImage(pos){
    setState(() {
      imageMultipleApiResponse.removeAt(pos);

    });
  }



  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        //selectedDate = picked.toString();
        selectedDate = DateFormat('yyyy-MM-dd').format(picked);   ///*  kk:mm*/
        dob.text=selectedDate.toString();
      });
  }
  loadsharedPrefrenceWithApi() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      callModelProfileApi(prefs.getString('token'));
    });
  }

  callModelProfileApi(String apptoken) {

    getModelProfileApi(apptoken).then((response) {
      if (response.statusCode == 200) {
        print(response.body);

        this.setState(() {
          final jsonResponse = json.decode(response.body);
          ModelProfileResponse modelProfileResponse = ModelProfileResponse.fromJson(jsonResponse);
          data = modelProfileResponse.data;
          fname.text=data.firstName;
          lname.text=data.lastName;
          email.text=data.email;
         // dob.text=data.dateOfBirth.toString();
          current_agency.text=data.currentAgency;
          mother_agency.text=data.motherAgency;
          hometown.text=data.address;
          country.text=data.country;
          instagram.text=data.socialInstagramLink;
          mobileNumber.text=data.phone;
          profileImage=data.profilePic;
          imageMultipleApiResponse=data.polaroids;

          var parsedDate = DateTime.parse(data.dateOfBirth.toString());
          var formatter = new DateFormat('yyyy-MM-dd');
          String formatted = formatter.format(parsedDate);
          dob.text=formatted.toString();
        });
      } else {
        print(response);
        MyUtils.showtoast("Something went wrong, please try again");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }



  updateProfileBtn(BuildContext context){
    ProgressDialog progressDialog =
    new ProgressDialog(context, type: ProgressDialogType.Normal);
    // progressDialog.setMessage("Please wait…");
  /*  progressDialog.update(
      progress: 50.0,
      message: "Please wait...",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()),
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
          color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
      messageTextStyle: TextStyle(
          color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600),
    );*/
    progressDialog.style(
        message: 'Please wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    progressDialog.show();
    List<String> polaroids=new List();
  /*  polaroids.add("P4CdmbcrpCqoWEWeB9TqBi7A1IObeIC0aep0849f.jpeg");
    polaroids.add("uRVJRwuPs1gNP4GXeyXOXyDdfPyjRJU2XKRNgjq9.jpeg");
*/
    var req  ={
      "first_name": fname.text,
      "last_name": lname.text,
      "phone": mobileNumber.text,
      "date_of_birth": dob.text,
      "current_agency": current_agency.text,
      "mother_agency": mother_agency.text,
      "address": hometown.text,
      "country": country.text,
      "social_instagram_link": instagram.text,
      "profile_pic": profileImage,
      "polaroids":imageMultipleApiResponse /*imageListMultiple*/  ,
    };
    var jsonReqString = json.encode(req);
    print(jsonReqString);

    var apicall;
    if(prefs.getString('user_type')=='model'){
      apicall=  updateModelProfileApi(jsonReqString,prefs.getString('token'));
    }else{
     // apicall= partnerChangePasswordApi(jsonReqString,prefs.getString('token'));
    }
    apicall.then((response) {
      if (response.statusCode == 200) {
        progressDialog.hide();
        print(response.body);
        final jsonResponse = json.decode(response.body);
        print(jsonResponse);

        MyUtils.showtoast(jsonResponse['message'].toString());
        Navigator.pop(context);
      } else {
        progressDialog.hide();
        print(response.statusCode);
        MyUtils.showtoast("Something went wrong, please try after some time");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }
/*
  _getImageList() async {
    var resultList = await MultiImagePicker.pickImages(
      maxImages :  10 ,
      enableCamera: true,
    );

    // The data selected here comes back in the list
    print(resultList);
    for ( var imageFile in resultList) {
  */
/*    postImage(imageFile).then((downloadUrl) {
        // Get the download URL
        print(downloadUrl.toString());
      }).catchError((err) {
        print(err);
      });*//*

    }
  }
*/



  Widget buildGridView() {
  /*  return GridView.count(
      crossAxisCount: 3,      children: List.generate(multiple_images.length, (index) {
      Asset asset = multiple_images[index];        return ViewImages(
        index,          asset,          key: UniqueKey(),        );      }),    ); */ }

  Future<void> loadAssets() async {
/*    setState(() {
      multiple_images = List<Asset>();    });
    List<Asset> resultList = List<Asset>();    String error = 'No Error Dectected';
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,        enableCamera: false,
        options: CupertinoOptions(takePhotoIcon: "chat"),      );    }  catch (e) {
      error = e.message;    }
    if (!mounted) return;
    setState(() {
      multiple_images = resultList;      _error = error;    }); */ }

  @override
  userImage(File _image) {
    print(_image);
    setState(() {
      this._image = _image;

    });
    var filesList=List<String>();
    filesList.add(_image.path);
    uploadmultipleimage(filesList,prefs.getString('token'));
   // uploadFile(,prefs.getString('token'));
  }
  Uint8List loadData() {
    File file = File(_image.path.toString());
    Uint8List bytes = file.readAsBytesSync();
    return bytes;
  }



  Future uploadmultipleimage(List images,apptoken) async {
    String apiUrl = '$BASE_URL/model/profile/image';
    var uri = Uri.parse(apiUrl);
    http.MultipartRequest request = new http.MultipartRequest('POST', uri);
    request.headers['XAPIKEY'] = MyConstants.XAPIKEY;
    request.headers['APPTOKEN'] = apptoken;
   // request.fields['user_id'] = '10';
   // request.fields['post_details'] = 'dfsfdsfsd';
    //multipartFile = new http.MultipartFile("imagefile", stream, length, filename: basename(imageFile.path));
    List<MultipartFile> newList = new List<MultipartFile>();
    for (int i = 0; i < images.length; i++) {
      File imageFile = File(images[i].toString());
      var stream =
      new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
      var length = await imageFile.length();
      var multipartFile = new http.MultipartFile("file", stream, length,
          filename: basename(imageFile.path));
      newList.add(multipartFile);
    }
    request.files.addAll(newList);
    var response = await request.send();
    if (response.statusCode == 200) {
      print("Image Uploaded");
    } else {
      print("Upload Failed");
    }
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
      final jsonResponse = json.decode(value);
      if(imageType=="profile_pic"){
        profileImage=jsonResponse['data']['filename'].toString();
      }else{
        setState(() {
          imageMultipleApiResponse.add(jsonResponse['data']['filename'].toString());

        });
      }
     print(imageMultipleApiResponse);
      MyUtils.showtoast(jsonResponse['message'].toString());


    });
  }


  initMultiPickUp() async {
    setState(() {
      images = null;
      _platformMessage = 'No Error';
    });
    List resultList;
    String error;
    try {
      resultList = await FlutterMultipleImagePicker.pickMultiImages(
          maxImageNo, selectSingleImage);
    } on PlatformException catch (e) {
      error = e.message;
    }

    if (!mounted) return;

    setState(() {
      images = resultList;

      for(int i=0;i<images.length;i++){
        var filesListMultiple= List<String>();
        filesListMultiple.add(images[i]);
        uploadmultipleimage(filesListMultiple,prefs.getString('token'));

      }

      print(images);
      if (error == null) _platformMessage = 'No Error Dectected';
    });
  }


}
