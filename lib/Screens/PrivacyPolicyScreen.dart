import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:webview_flutter/webview_flutter.dart';

class _PrivacyPolicyScreenState extends State < PrivacyPolicyScreen > {
 // var _url;
  final _key = UniqueKey();
  _PrivacyPolicyScreenState();
  num _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: toolbarLayout("Privacy Policy"),
        body: IndexedStack(
          index: _stackToView,
          children: [
            Column(
              children: < Widget > [
                Expanded(
                    child: WebView(
                      //key: _key,
                      javascriptMode: JavascriptMode.unrestricted,
                      initialUrl: widget.url,
                      onPageFinished: _handleLoad,
                    )
                ),
              ],
            ),
            Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ],
        )
    );
  }
}

class PrivacyPolicyScreen extends StatefulWidget {
  final url=MyConstants.PRIVACY_POLICY_URL;
  @override
  createState() => _PrivacyPolicyScreenState();
}