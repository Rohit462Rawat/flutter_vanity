import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SubPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sub Page'),
        backgroundColor: Colors.redAccent,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/logout.png'),
            Text('Click button to back to Main Page'),
              RaisedButton(
              //  margin: EdgeInsets.only(left:10,top:10,right:10,bottom: 50 ),
              //  padding: EdgeInsets.all(30.0),
          textColor: Colors.white,
          color: Colors.redAccent,
          child: Text('Open Dialog'),
          onPressed: () {
            _asyncConfirmDialog(context);
          },
        ),
            RaisedButton(
              textColor: Colors.white,
              color: Colors.redAccent,
              child: Text('Back to Main Page'),
              onPressed: () {
                // TODO
                Navigator.pop(context);
              },
            )
          ],
        ),
      ),
    );
  }

    Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
    return AlertDialog(
    title: Text('Reset settings?'),
    content: const Text(
    'This will reset your device to its default factory settings.'),
    actions: <Widget>[
    FlatButton(
    child: const Text('CANCEL'),
    onPressed: () {
    Navigator.of(context).pop(ConfirmAction.CANCEL);
    },
    ),
    FlatButton(
    child: const Text('ACCEPT'),
    onPressed: () {
    Navigator.of(context).pop(ConfirmAction.ACCEPT);
    },
    )
    ],
    );
    },
    );
    }
  }


enum ConfirmAction { CANCEL, ACCEPT }
