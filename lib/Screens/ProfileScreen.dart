import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:intl/intl.dart';


class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}
class _LoginData {
  String email = '';
  String password = '';
  String first_name = '';

}
class _ProfileScreenState extends State<ProfileScreen> {
  String selectedDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar:  AppBar(
         centerTitle: true,
         title: Text("Profile"),
         backgroundColor: Colors.pink,
       ),
      body: _buildUIComposer(),);
  }

  Widget _buildUIComposer() {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: new BoxDecoration(
        image: DecorationImage(
            image: new ExactAssetImage('assets/images/login_bg.png'),
            fit: BoxFit.fill),
      ),
      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(
              left: 20, top: 20, right: 20, bottom: 20),
          child: Column(
            children: <Widget>[
              Row(
                  children: <Widget>[
                    Expanded(child: textFieldBackgroundUI(Image.asset('assets/images/username_icon.png'),
                        TextField( onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'First Name',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),),
                    SizedBox(width: 15,),
                    Expanded(child: textFieldBackgroundUI(Icon(Icons.email),
                        TextField( onChanged: (text) {
                          // email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Last Name',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),)
                  ]),
              Row(
                  children: <Widget>[
                    Expanded(child: textFieldBackgroundUI(
                        Icon(Icons.email),
                        TextField(onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Email',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),),
                    SizedBox(width: 15,),
                    Expanded(
                      child: textFieldBackgroundUI(Icon(Icons.calendar_today),
                          InkWell(
                            onTap: () {
                              _selectDate(context);   // Call Function that has showDatePicker()
                            },
                            child: IgnorePointer(
                              child: new TextField(onChanged: (text) {
                                // email = text;
                              },
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.done,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'DOB',
                                    labelText: selectedDate,
                                    hintStyle: TextStyle(fontSize: 14)
                                )
                                //flexible

                                ,
                              ),
                            ),

                          )),)
                  ]),
              Row(
                  children: <Widget>[
                    Expanded(child: textFieldBackgroundUI(
                        Icon(Icons.email),
                        TextField(onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Current Agency',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),),
                    SizedBox(width: 15,),
                    Expanded(child: textFieldBackgroundUI(Icon(Icons.email),
                        TextField(onChanged: (text) {
                          // email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Mother Agency',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),)
                  ]),
              Row(
                  children: <Widget>[
                    Expanded(child: textFieldBackgroundUI(
                        Icon(Icons.email),
                        TextField(onChanged: (text) {
                          //email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Home Town',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),),
                    SizedBox(width: 15,),
                    Expanded(child: textFieldBackgroundUI(Icon(Icons.email),
                        TextField(onChanged: (text) {
                          // email = text;
                        },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Country',
                                hintStyle: TextStyle(fontSize: 14)
                            )
                          //flexible

                        )),)
                  ]),
              textFieldBackgroundUI(Icon(Icons.email),
                  TextField(onChanged: (text) {
                    // email = text;
                  },
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Instagram',
                          hintStyle: TextStyle(fontSize: 14)
                      )
                    //flexible

                  )),
              textFieldBackgroundUI(Icon(Icons.email),
                  TextField(onChanged: (text) {
                    // email = text;
                  },
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Mobile Number',
                          hintStyle: TextStyle(fontSize: 14)
                      )
                    //flexible

                  )),
              textFieldBackgroundUI(Icon(Icons.email),
                  TextField(onChanged: (text) {
                    // email = text;
                  },
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      obscureText: true,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Password',
                          hintStyle: TextStyle(fontSize: 14)
                      )
                    //flexible

                  )),
              textFieldBackgroundUI1(Icon(Icons.file_upload,color: Colors.pink,),
                  Text("Upload Newest Polaroids",style: TextStyle(color: Colors.pink),)),

              Container(margin: EdgeInsets.fromLTRB(20,20,20,20),
                  child:Text("Vanity Pass does not gurentee you membership when you fill out this information",textAlign:TextAlign.center,style: TextStyle(fontStyle: FontStyle.italic),)),
              Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.only(
                      top: 30,  bottom: 20),
                  // padding: const EdgeInsets.all(3.0),

                  decoration: new BoxDecoration(
                    color: Colors.pink,
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(30.0),
                    ),
                  ),
                  child: FlatButton(
                      onPressed: null,
                      child: Text("Next",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontStyle: FontStyle.normal))))
            ],
          ),
        ),
      ),
    );
  }



  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        //selectedDate = picked.toString();
        selectedDate = DateFormat('yyyy-MM-dd  kk:mm').format(picked);

      });
  }
}
