import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyUIs.dart';


class ContactUsScreen extends StatefulWidget {
  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {

  var message;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: toolbarLayout("Contact Us"),
      body: bodyUI(),

    );
  }
  bodyUI(){
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.topLeft,
          height: 110,
          margin: const EdgeInsets.only(left: 20, top: 30, right: 20),
          // padding: const EdgeInsets.all(3.0),
          decoration: new BoxDecoration(
            border: new Border.all(color: Colors.grey),
            borderRadius: const BorderRadius.all(
              const Radius.circular(30.0),
            ),
          ),
          child:  Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(
                      left: 12, top: 10, right: 10, bottom: 10),
                  child:  Align(alignment:Alignment.topLeft,child: Icon(Icons.message))),
              Container(
                child:  Flexible(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: TextField(
                      autofocus: true,

                      textAlign: TextAlign.start,
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      onChanged: (text) {
                        message = text;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(

                        border: InputBorder.none,
                        hintText: 'Message',
                      ),
                    ),
                  ),
                ), //flexible
              ), //container
            ], //widget
          ),),

        Container(
            height: 50,
            width: double.infinity,
            margin: const EdgeInsets.only(left: 20, top: 30, right: 20),
            // padding: const EdgeInsets.all(3.0),

            decoration: new BoxDecoration(
              color: Colors.pink,
              borderRadius: const BorderRadius.all(
                const Radius.circular(30.0),
              ),
            ),
            child: FlatButton(
                onPressed: null,
                child: Text("Contact Us",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontStyle: FontStyle.normal))))
      ],
    );


  }
}
