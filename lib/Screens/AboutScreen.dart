import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:webview_flutter/webview_flutter.dart';


class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {

  @override
  void initState() {
   // flutterWebviewPlugin.onDestroy.listen((_) => exit(0));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return  new Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('About Us'),
          backgroundColor: Colors.pink,
        ),
     body: bodyUI()/*new WebviewScaffold(
       url: MyConstants.ABOUT_US_URL,
       appBar: new AppBar(
         title: new Text("About US"),
         leading: new IconButton(
           icon: new Icon(Icons.keyboard_backspace),
           onPressed: () => Navigator.of(context).pop(),
         ),
       ),
       withZoom: true,
       withLocalStorage: true,
       hidden: true,
       initialChild: Container(
         color: Colors.redAccent,
         child: const Center(
           child: Text('Waiting.....'),
         ),
       ),
     )*/
    );
  }

  bodyUI(){
    return  Container(
      child: WebView(
        initialUrl: MyConstants.ABOUT_US_URL,
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }



}
