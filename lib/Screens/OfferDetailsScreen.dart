import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_demo/model/ModelTimeAvailabilityResponse.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/model/modelOfferList/ModelOfferListResponse.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OfferDetailsScreen extends StatefulWidget {

  OfferListData dataList;
  OfferDetailsScreen({this.dataList}) ;

  @override
  _OfferDetailsScreenState createState() => _OfferDetailsScreenState();
}

class _OfferDetailsScreenState extends State<OfferDetailsScreen> {

  SharedPreferences prefs;
  List<TimeAvailabiltyData> timeAvailabiltyList;


  @override
  void initState() {
    loadsharedPrefrenceWithApi();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.dataList.name),
        backgroundColor: Colors.pink,
      ),
      body: BodyUI(widget.dataList),
    );
  }

  BodyUI(dataList){
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Row(
                //mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      //padding: EdgeInsets.all(10),
                      child: CircleAvatar(
                        radius: 40.0,
                        backgroundImage: NetworkImage(
                            MyConstants.EVENT_IMAGE +
                                dataList.thumbnail.toString()),
                        backgroundColor: Colors.grey,
                        /*child: FadeInImage.assetNetwork(
                                        placeholder: 'assets/images/loading.gif',
                                        image: MyConstants.SERVICE_IMAGE,
                                        width: 60,
                                        height: 60,
                                      ),*/
                      )
                    /*Image.network(
                                      MyConstants.SERVICE_IMAGE+datum.image,
                                      width: 60,
                                      height: 60,
                                      fit:BoxFit.fill )*/
                  ),
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.all(5),
                      padding: EdgeInsets.all(5),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(dataList.name.toUpperCase(),
                                  style: TextStyle(fontSize: 13,color: Colors.black,fontWeight: FontWeight.bold))),
                          SizedBox(height: 5),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(dataList.frequency.toUpperCase(),
                                  style: TextStyle(fontSize: 13,color: Colors.black,fontWeight: FontWeight.bold))),

                          SizedBox(height: 5),

                          ticketAvailabilityText(dataList),

                          SizedBox(height: 5,),

                        ],
                      ),
                    ),
                  ),
                ],
              ),
             SizedBox(height: 10),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(dataList.description,
                      style: TextStyle(fontSize: 13,color: Colors.black,fontWeight: FontWeight.bold))),
              SizedBox(height: 10),
              Container(
                width: double.infinity,
              
                child: RaisedButton(shape: RoundedRectangleBorder(
                    borderRadius:  BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0),bottomLeft:Radius.circular(20.0),bottomRight: Radius.circular(20.0) )),
                    child: Align(alignment: Alignment.topLeft,
                      child: Text("Time Availability List",
                          style: TextStyle(color: Colors.white,fontSize: 12 )),
                    ),
                    color:  Colors.pink,
                    disabledColor: Colors.pink),

              ),
              _listViewTimeAvailabiltyList()
            ],

          )),
    );

  }

  ticketAvailabilityText(offerList){
    if(offerList.ticketAvailable=="0") {
      return Text("SOLD OUT", style: TextStyle(fontSize: 13, color: Colors.red));
    }else{
      return Text("Available".toUpperCase(), style: TextStyle(fontSize: 13, color: Colors.green));
    }

  }

  Widget _listViewTimeAvailabiltyList() {
    return timeAvailabiltyList == null
        ? Center(
      child: Container(
          margin:EdgeInsets.fromLTRB(0, 10,0, 0),
          child: CircularProgressIndicator()),
    )
        : ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: timeAvailabiltyList.length,
        itemBuilder: (context, index) {
          //Datum datum = eventList.data[index];
          return InkWell(
            //onTap:(){ onCardTap(offerList[index]);},
            child: Card(
              elevation: 3,
              margin: EdgeInsets.fromLTRB(10,10,10,10),
              child: Container(
                  child: Row(
                    //mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                     /* Container(
                          padding: EdgeInsets.all(10),
                          child: CircleAvatar(
                            radius: 40.0,
                            backgroundImage: NetworkImage(
                                MyConstants.EVENT_IMAGE +
                                    timeAvailabiltyList[index].thumbnail.toString()),
                            backgroundColor: Colors.transparent,
*//*child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/loading.gif',
                                    image: MyConstants.SERVICE_IMAGE,
                                    width: 60,
                                    height: 60,
                                  ),*//*

                          )*//*
Image.network(
                                  MyConstants.SERVICE_IMAGE+datum.image,
                                  width: 60,
                                  height: 60,
                                  fit:BoxFit.fill )*//*

                      ),*/
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text("Day : "+timeAvailabiltyList[index].day.toUpperCase(),
                                      style: TextStyle(fontSize: 13,color: Colors.black,fontWeight: FontWeight.bold))),
                              SizedBox(height: 5),



                              Text("Start Time : "+timeAvailabiltyList[index].openingTime,
                                  style: TextStyle(fontSize: 12)),

                              SizedBox(height: 5),

                              Text("End Time : "+timeAvailabiltyList[index].closingTime,
                                  style: TextStyle(fontSize: 12)),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          );
        });
  }



  loadsharedPrefrenceWithApi() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      callModelOfferListApi(prefs.getString('token'));
    });
  }

  callModelOfferListApi(String apptoken) {

    getModelTimeAvailabilityList(apptoken,widget.dataList.id).then((response) {
      if (response.statusCode == 200) {
        print("time"+response.body);

        this.setState(() {
          final jsonResponse = json.decode(response.body);
          ModelTimeAvailabilityResponse modelTimeAvailabilityResponse = ModelTimeAvailabilityResponse.fromJson(jsonResponse);
          timeAvailabiltyList = modelTimeAvailabilityResponse.data;
        });
      } else {
        print(response.statusCode);
        MyUtils.showtoast("Something went wrong, please try again");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }
}
