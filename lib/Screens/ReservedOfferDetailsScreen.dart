import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/model/ModelTimeAvailabilityResponse.dart';
import 'package:flutter_demo/model/ReservedTicketResponse.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';



class ReservedOfferDetailsScreen extends StatefulWidget {
  ReservedTicketData dataList;
  ReservedOfferDetailsScreen({this.dataList}) ;

  @override
  _ReservedOfferDetailsScreenState createState() => _ReservedOfferDetailsScreenState();
}

class _ReservedOfferDetailsScreenState extends State<ReservedOfferDetailsScreen> {
  List<TimeAvailabiltyData> timeAvailabiltyList;
  SharedPreferences prefs;


  @override
  void initState() {
    loadsharedPrefrenceWithApi();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: toolbarLayout("Reserved Details"),
      body:bodyUI() ,
    );
  }

  bodyUI(){
    print(widget.dataList);
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Container(
              height: 150,
                padding: EdgeInsets.all(10),
                  child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/loading.gif',
                                    image: MyConstants.QRCODE_END_URL+widget.dataList.qrCode.toString().replaceAll("svg", "png"),
                    height: 150,
                    width: 120,

                                  ),

            ),
            SizedBox(height: 10,),
          Align(alignment: Alignment.center,
            child:
          Text("Please do not forget to post on instagram and tag the venue & tag @VanityPass.",textAlign:TextAlign.center,style: TextStyle(color: Colors.pink),)
            ,)
          ,SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    width: 80,
                    height: 25,
                    child: RaisedButton(shape: RoundedRectangleBorder(
                        borderRadius:  BorderRadius.only(
                            topLeft: Radius.circular(15.0),
                            topRight: Radius.circular(0),bottomLeft:Radius.circular(15.0),bottomRight: Radius.circular(0) )),
                        //onPressed: listView_onPressed,
                        child: Text(
                          "Offer",
                          style: TextStyle(
                              color: Colors.white,fontSize: 10),
                        ),
                        color: Colors.pink ,
                        disabledColor:
                        Colors.pink)),
                Container(
                  width: 80,
                  height: 25,
                  child: RaisedButton(shape: RoundedRectangleBorder(
                      borderRadius:  BorderRadius.only(
                          topLeft: Radius.circular(0),
                          topRight: Radius.circular(15.0),bottomLeft:Radius.circular(0),bottomRight: Radius.circular(15.0) )),
                     // onPressed: onVenueButtonClicked,
                      child: Text("Venue",
                          style: TextStyle(
                              color: Colors.black,fontSize: 10)),
                      color: Colors.white,
                      disabledColor:
                      Colors.white),
                ),
              ],
            ),
            SizedBox(height: 10,),
            Card(
              elevation: 3,
              margin: EdgeInsets.all(5),
              
              child: Container(
                margin: EdgeInsets.fromLTRB(10,20,10,20),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Offer Name",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Colors.pink),),
                    SizedBox(height: 5,),
                    Text(widget.dataList.offerName,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.black),),
                    SizedBox(height: 10,),
                    Text("Offer Description",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: Colors.pink),),
                    SizedBox(height: 5,),
                    Text(widget.dataList.offerDescription,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12,color: Colors.black),),


                  ],

                ),
              ),
            ),

            SizedBox(height: 20,),

            Container(
              width: double.infinity,

              child: RaisedButton(shape: RoundedRectangleBorder(
                  borderRadius:  BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),bottomLeft:Radius.circular(20.0),bottomRight: Radius.circular(20.0) )),
                  child: Align(alignment: Alignment.topLeft,
                    child: Text("Time Availability List",
                        style: TextStyle(color: Colors.white,fontSize: 12 )),
                  ),
                  color:  Colors.pink,
                  disabledColor: Colors.pink),

            ),
            _listViewTimeAvailabiltyList()

          ],
        ),
      ),
    );
  }
  Widget _listViewTimeAvailabiltyList() {
    return timeAvailabiltyList == null
        ? Center(
      child: Container(
          margin:EdgeInsets.fromLTRB(0, 10,0, 0),
          child: CircularProgressIndicator()),
    )
        : ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: timeAvailabiltyList.length,
        itemBuilder: (context, index) {
          //Datum datum = eventList.data[index];
          return InkWell(
            //onTap:(){ onCardTap(offerList[index]);},
            child: Card(
              elevation: 3,
              margin: EdgeInsets.fromLTRB(10,10,10,10),
              child: Container(
                  child: Row(
                    //mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      /* Container(
                          padding: EdgeInsets.all(10),
                          child: CircleAvatar(
                            radius: 40.0,
                            backgroundImage: NetworkImage(
                                MyConstants.EVENT_IMAGE +
                                    timeAvailabiltyList[index].thumbnail.toString()),
                            backgroundColor: Colors.transparent,
*//*child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/loading.gif',
                                    image: MyConstants.SERVICE_IMAGE,
                                    width: 60,
                                    height: 60,
                                  ),*//*

                          )*//*
Image.network(
                                  MyConstants.SERVICE_IMAGE+datum.image,
                                  width: 60,
                                  height: 60,
                                  fit:BoxFit.fill )*//*

                      ),*/
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text("Day : "+timeAvailabiltyList[index].day.toUpperCase(),
                                      style: TextStyle(fontSize: 13,color: Colors.black,fontWeight: FontWeight.bold))),
                              SizedBox(height: 5),



                              Text("Start Time : "+timeAvailabiltyList[index].openingTime,
                                  style: TextStyle(fontSize: 12)),

                              SizedBox(height: 5),

                              Text("End Time : "+timeAvailabiltyList[index].closingTime,
                                  style: TextStyle(fontSize: 12)),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          );
        });
  }


  loadsharedPrefrenceWithApi() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      getModelTimeAvailabilityListApi(prefs.getString('token'));
    });
  }

  getModelTimeAvailabilityListApi(String apptoken) {

    getModelTimeAvailabilityList(apptoken,widget.dataList.eventOfferId).then((response) {
      if (response.statusCode == 200) {
        print("time"+response.body);

        this.setState(() {
          final jsonResponse = json.decode(response.body);
          ModelTimeAvailabilityResponse modelTimeAvailabilityResponse = ModelTimeAvailabilityResponse.fromJson(jsonResponse);
          timeAvailabiltyList = modelTimeAvailabilityResponse.data;
        });
      } else {
        print(response.statusCode);
        MyUtils.showtoast("Something went wrong, please try again");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }
}
