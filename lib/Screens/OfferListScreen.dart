import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_demo/Screens/OfferDetailsScreen.dart';
import 'package:flutter_demo/Screens/VenueScreen.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/model/PartnerEventList_model.dart';
import 'package:flutter_demo/model/modelOfferList/ModelOfferListRequest.dart';
import 'package:flutter_demo/model/modelOfferList/ModelOfferListResponse.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';


class OfferListScreen extends StatefulWidget {
   Datum offerDataList;
   var distance,latitude,longitude;
  OfferListScreen({this.offerDataList,this.distance,this.latitude,this.longitude}) ;

  @override
  _OfferListScreenState createState() => _OfferListScreenState();
}



class _OfferListScreenState extends State<OfferListScreen> {

  SharedPreferences prefs;
  List<OfferListData> offerList;

  @override
  void initState() {
    loadsharedPrefrenceWithApi();
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          centerTitle: true,
          title: Text(widget.offerDataList.name),
          backgroundColor: Colors.pink,
        ),
      body: BodyUI(widget.offerDataList.galleries),
    );
  }

  CarouselSlider carouselSlider;
  int _current = 0;


  List imgList =/*widget.dataList.galleries;*/ [
    'https://images.unsplash.com/photo-1502117859338-fd9daa518a9a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1554321586-92083ba0a115?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1536679545597-c2e5e1946495?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1543922596-b3bbaba80649?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1502943693086-33b5b1cfdf2f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80'
  ];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  BodyUI(List<dynamic> galleries){
    //print(galleries);
   return SingleChildScrollView(
     child: Container(
       margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
       width: double.infinity,
        child: Column(
         //mainAxisSize: MainAxisSize.max,
          //mainAxisAlignment: MainAxisAlignment.center,
         // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
           galleries.length==0?SizedBox(height: 200,child: Center(child: Text("No gallery availoable"),),): //imageSliderwithIndicator(galleries),
            CarouselSlider(
              // aspectRatio: 2,
              height: 180.0,
              initialPage: 0,
              enlargeCenterPage: true,
              autoPlay: true,
              reverse: false,
              enableInfiniteScroll: true,
              autoPlayInterval: Duration(seconds: 2),
              autoPlayAnimationDuration: Duration(milliseconds: 2000),
              pauseAutoPlayOnTouch: Duration(seconds: 10),
              scrollDirection: Axis.horizontal,
              onPageChanged: (index) {
                setState(() {
                  _current = index;
                });
              },
              items: galleries.map((imgUrl) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Image.network(
                        MyConstants.EVENT_IMAGE+imgUrl,
                        fit: BoxFit.fill,
                      ),
                    );
                  },
                );
              }).toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: map<Widget>(galleries, (index, url) {
                return Container(
                  width: 10.0,
                  height: 10.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == index ? Colors.pink : Colors.black,
                  ),
                );
              }),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              margin: EdgeInsets.all(10),
              alignment: Alignment.topLeft,
              child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start
              ,children: <Widget>[
                Text("VENUE NAME",style: TextStyle(color: Colors.pink,fontSize: 12,fontStyle: FontStyle.normal),),
                SizedBox(height: 5,),
                Text(widget.offerDataList.name,style: TextStyle(color: Colors.black,fontSize: 12),),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                  height: 1,
                  color:  Colors.grey,
                ),
                SizedBox(height: 5,),
                Text("VENUE DESCRIPTION",style: TextStyle(color: Colors.pink,fontSize: 12,fontStyle: FontStyle.normal),),
                SizedBox(height: 5,),
                Text(widget.offerDataList.description,style: TextStyle(color: Colors.black,fontSize: 12),),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                  height: 1,
                  color:  Colors.grey,
                ),
                SizedBox(height: 20,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        width: 80,
                        height: 25,
                        child: RaisedButton(shape: RoundedRectangleBorder(
                            borderRadius:  BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                            topRight: Radius.circular(0),bottomLeft:Radius.circular(15.0),bottomRight: Radius.circular(0) )),
                           //onPressed: listView_onPressed,
                            child: Text(
                              "Offer",
                              style: TextStyle(
                                  color: Colors.white,fontSize: 10),
                            ),
                            color: Colors.pink ,
                            disabledColor:
                             Colors.pink)),
                    Container(
                      width: 80,
                      height: 25,
                      child: RaisedButton(shape: RoundedRectangleBorder(
                          borderRadius:  BorderRadius.only(
                              topLeft: Radius.circular(0),
                              topRight: Radius.circular(15.0),bottomLeft:Radius.circular(0),bottomRight: Radius.circular(15.0) )),
                               onPressed: onVenueButtonClicked,
                          child: Text("Venue",
                              style: TextStyle(
                                  color: Colors.black,fontSize: 10)),
                          color: Colors.white,
                          disabledColor:
                          Colors.white),
                    ),
                  ],
                ),


                Container(margin:EdgeInsets.fromLTRB(0, 10,0, 0)
                    ,child: _listViewOfferList())

              ],))
          ],
        ),
      ),
   );
  }




  bodyUi(galleries) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
        width: double.infinity,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate(
                [

                  /*galleries.length==0?SizedBox(height: 180,):
                  carouselSlider = CarouselSlider(
                    // aspectRatio: 2,
                    height: 180.0,
                    initialPage: 0,
                    enlargeCenterPage: true,
                    autoPlay: true,
                    reverse: false,
                    enableInfiniteScroll: true,
                    autoPlayInterval: Duration(seconds: 2),
                    autoPlayAnimationDuration: Duration(milliseconds: 2000),
                    pauseAutoPlayOnTouch: Duration(seconds: 10),
                    scrollDirection: Axis.horizontal,
                    onPageChanged: (index) {
                      setState(() {
                        _current = index;
                      });
                    },
                    items: galleries.map((imgUrl) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            decoration: BoxDecoration(
                              color: Colors.green,
                            ),
                            child: Image.network(
                              MyConstants.EVENT_IMAGE+imgUrl,
                              fit: BoxFit.fill,
                            ),
                          );
                        },
                      );
                    }).toList(),
                  )*/

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: map<Widget>(widget.offerDataList.galleries.length==0?0:galleries, (index, url) {
                      return Container(
                        width: 10.0,
                        height: 10.0,
                        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _current == index ? Colors.redAccent : Colors.green,
                        ),
                      );
                    }),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                      margin: EdgeInsets.all(10),
                      alignment: Alignment.topLeft,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                        Text("VENUE NAME",style: TextStyle(color: Colors.pink,fontSize: 12,fontStyle: FontStyle.normal),),
                        SizedBox(height: 5,),
                        Text(widget.offerDataList.name,style: TextStyle(color: Colors.black,fontSize: 12),),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                          height: 1,
                          color:  Colors.grey,
                        ),
                        SizedBox(height: 5,),
                        Text("VENUE DESCRIPTION",style: TextStyle(color: Colors.pink,fontSize: 12,fontStyle: FontStyle.normal),),
                        SizedBox(height: 5,),
                        Text(widget.offerDataList.description,style: TextStyle(color: Colors.black,fontSize: 12),),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                          height: 1,
                          color:  Colors.grey,
                        ),
                        SizedBox(height: 20,),

                        Container(margin:EdgeInsets.fromLTRB(0, 0, 0, 50)
                            ,child: _listViewOfferList())

                      ],))
                ],
              ),
            ),
          ],
        )
    );
  }





  loadsharedPrefrenceWithApi() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      callModelOfferListApi(prefs.getString('token'));
    });
  }

  callModelOfferListApi(String apptoken) {
    ModelOfferListRequest Req = ModelOfferListRequest(
        eventId: int.parse(widget.offerDataList.id),
        page: "1",
        limit: '10',
        timezone: 'Asia/Calcutta');

    getModelOfferList(apptoken, Req).then((response) {
      if (response.statusCode == 200) {
        print("ghf"+response.body);

        this.setState(() {
          final jsonResponse = json.decode(response.body);
          ModelOfferListResponse offerListResponse = ModelOfferListResponse.fromJson(jsonResponse);
              /* for(int i=0;i<partnerEventList.data.length-1;i++) {
            eventList.add(offerListResponse.data[i]);
          }*/
          offerList = offerListResponse.data;
        });
      } else {
        print(response.statusCode);
        MyUtils.showtoast("Something went wrong, please try again");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }

  Widget _listViewOfferList() {
    return offerList == null
        ? Center(
      child: Container(
          margin:EdgeInsets.fromLTRB(0, 10,0, 0),
          child: CircularProgressIndicator()),
    )
        : ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: offerList.length,
        itemBuilder: (context, index) {
          //Datum datum = eventList.data[index];
          return InkWell(
            onTap:(){ onCardTap(offerList[index],widget.offerDataList);},
            child: Card(
              elevation: 3,
              margin: EdgeInsets.fromLTRB(10,10,10,10),
              child: Container(
                  child: Row(
                    //mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.all(10),
                          child: CircleAvatar(
                            radius: 40.0,
                            backgroundImage: NetworkImage(
                                MyConstants.EVENT_IMAGE +
                                    offerList[index].thumbnail.toString()),
                            backgroundColor: Colors.grey,
                            /*child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/loading.gif',
                                    image: MyConstants.SERVICE_IMAGE,
                                    width: 60,
                                    height: 60,
                                  ),*/
                          )
                        /*Image.network(
                                  MyConstants.SERVICE_IMAGE+datum.image,
                                  width: 60,
                                  height: 60,
                                  fit:BoxFit.fill )*/
                      ),
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(offerList[index].name.toUpperCase(),
                                      style: TextStyle(fontSize: 13,color: Colors.pink,fontWeight: FontWeight.bold))),
                              SizedBox(height: 5),

                              ticketAvailabilityText(offerList,index),

                              SizedBox(height: 5,),

                             Text(offerList[index].description,
                                  style: TextStyle(fontSize: 12)),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          );
        });
  }

  ticketAvailabilityText(offerList,index){
    if(offerList[index].ticketAvailable=="0") {
      return Text("SOLD OUT", style: TextStyle(fontSize: 13, color: Colors.red));
    }else{
      return Text("Available".toUpperCase(), style: TextStyle(fontSize: 13, color: Colors.green));
    }

  }

  onCardTap(OfferListData data,offerDataList){
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => OfferDetailsScreen(dataList:data)));
  }
  onVenueButtonClicked(){
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => VenueScreen(distance:widget.distance,latitude:widget.latitude,longitude:widget.longitude)));
  }

}
