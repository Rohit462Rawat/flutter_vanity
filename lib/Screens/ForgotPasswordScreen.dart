import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyUIs.dart';


class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  String email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
        title: new Text('Forgot Password'),
    ),
       body: _buildUIComposer());
  }

  Widget _buildUIComposer() {
    return Container(
      height: double.infinity,
      decoration: new BoxDecoration(
        image: DecorationImage(
            image: new ExactAssetImage('assets/images/login_bg.png'),
            fit: BoxFit.fill),
      ),
      child: SingleChildScrollView(
        child: Container(
            margin: const EdgeInsets.only(
                left: 20, right: 20, bottom: 20),
          child: Column(
            children: <Widget>[
              Container(
                  height: 150,
                  margin: const EdgeInsets.only( top: 80, ),
                  // padding: const EdgeInsets.all(3.0),
                  child: new Image.asset('assets/images/app_icon.png')),
                SizedBox(height: 70,),
                textFieldBackgroundUI(Icon(Icons.person),
                       TextField(
                          onChanged: (text) {
                            email = text;
                          },
                           keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Email',
                          )
                       //flexible

                   ),),
              Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.only(
                      left: 20, top: 30, right: 20, bottom: 20),
                  // padding: const EdgeInsets.all(3.0),

                  decoration: new BoxDecoration(
                    color: Colors.pink,
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(30.0),
                    ),
                  ),
                  child: FlatButton(
                      onPressed: forgetPasswordSubmitButton,
                      child: Text("Submit",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontStyle: FontStyle.normal))))
            ],
          ),
        ),
      ),
    );
  }

  forgetPasswordSubmitButton(){
    print(email);

  }

}
