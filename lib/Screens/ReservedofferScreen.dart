import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_demo/Screens/ReservedOfferDetailsScreen.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/model/ReservedTicketResponse.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReservedofferScreen extends StatefulWidget {
  @override
  _ReservedofferScreenState createState() => _ReservedofferScreenState();
}

class _ReservedofferScreenState extends State<ReservedofferScreen> {

  SharedPreferences prefs;
  List<ReservedTicketData> reservedTicketsList;


  @override
  void initState() {
    loadsharedPrefrenceWithApi();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar:
      toolbarLayout("Reserved Offers"),
    body: Card(
        elevation: 3,
        margin: EdgeInsets.fromLTRB(10,10,10,10),
        child: _listView()),
    );
  }

  Widget _listView() {
    return reservedTicketsList == null
        ? Center(
      child: Container(
          margin:EdgeInsets.fromLTRB(0, 0,0, 0),
          child: CircularProgressIndicator()),
    )
        : ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: reservedTicketsList.length,
        itemBuilder: (context, index) {
          //Datum datum = eventList.data[index];
          return InkWell(
            onTap:(){ onCardTap(reservedTicketsList[index]);},
              child: Container(
                  child: Row(
                    //mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[

                      Flexible(
                        child: Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[

                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text("Venue Name : "+reservedTicketsList[index].eventName,
                                      style: TextStyle(fontSize: 13,color: Colors.pink,fontWeight: FontWeight.bold))),
                              SizedBox(height: 5),



                              Text("Offer Name : "+reservedTicketsList[index].offerName,
                                  style: TextStyle(fontSize: 12)),

                              SizedBox(height: 5),

                              Text("Status : "+reservedTicketsList[index].status,
                                  style: TextStyle(fontSize: 12)),
                              SizedBox(height: 5),
                              Divider(
                                color: Colors.grey,
                                height: 1,
                              ),
                            ],
                          ),
                        ),
                      ),


                    ],
                  )),

          );
        });
  }



  loadsharedPrefrenceWithApi() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      callReservedOffersApi(prefs.getString('token'));
    });
  }

  callReservedOffersApi(String apptoken) {

    getReserverTicketsApi(apptoken).then((response) {
      if (response.statusCode == 200) {
        print(response.body);

        this.setState(() {
          final jsonResponse = json.decode(response.body);
          ReservedTicketResponse modelTimeAvailabilityResponse = ReservedTicketResponse.fromJson(jsonResponse);
          reservedTicketsList = modelTimeAvailabilityResponse.data;
        });
      } else {
        print(response);
        MyUtils.showtoast("Something went wrong, please try again");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }

  onCardTap(ReservedTicketData reservedTicketData){
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => ReservedOfferDetailsScreen(dataList:reservedTicketData)));
  }

}
