import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:webview_flutter/webview_flutter.dart';


class TermsOfServicesScreen extends StatefulWidget {
  final url=MyConstants.TERMS_OF_USE_URL;
  @override
  createState() => _TermsOfServicesScreenState();
}

class _TermsOfServicesScreenState extends State < TermsOfServicesScreen > {
  // var _url;
  final _key = UniqueKey();
  _TermsOfServicesScreenState();
  num _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: toolbarLayout("Terms & Conditions"),
        body: IndexedStack(
          index: _stackToView,
          children: [
            Column(
              children: < Widget > [
                Expanded(
                    child: WebView(
                      //key: _key,
                      javascriptMode: JavascriptMode.unrestricted,
                      initialUrl: widget.url,
                      onPageFinished: _handleLoad,
                    )
                ),
              ],
            ),
            Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ],
        )
    );
  }
}

