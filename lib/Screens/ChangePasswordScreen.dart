import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {

  SharedPreferences prefs;
  String oldPass, newPass,confirmPass;

  @override
  void initState() {
    super.initState();
    addStringToSF();

  }

  addStringToSF() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: toolbarLayout("Change Password"),
        body: Container(
            height: double.infinity,
            decoration: new BoxDecoration(
              image: DecorationImage(
                  image: new ExactAssetImage('assets/images/login_bg.png'),
                  fit: BoxFit.fill),
            ),
            child: SingleChildScrollView(child: _buildUIComposer())));
  }

  Widget _buildUIComposer() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              height: 150,
              margin: const EdgeInsets.only(left: 20, top: 80, right: 20),
              // padding: const EdgeInsets.all(3.0),

              child: new Image.asset('assets/images/app_icon.png')),
          Container(
            height: 50,
            margin: const EdgeInsets.only(left: 20, top: 50, right: 20),
            // padding: const EdgeInsets.all(3.0),
            decoration: new BoxDecoration(
              border: new Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(
                const Radius.circular(30.0),
              ),
            ),
            child: new Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(
                        left: 20, top: 10, right: 10, bottom: 10),
                    child: new Icon(Icons.lock)),
                new Container(
                  child: new Flexible(
                    child: new TextField(
                      obscureText: true,
                      onChanged: (text) {

                        oldPass = text;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Old Password',
                      ),
                    ),
                  ), //flexible
                ), //container
              ], //widget
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
            // padding: const EdgeInsets.all(3.0),
            decoration: new BoxDecoration(
              border: new Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(
                const Radius.circular(30.0),
              ),
            ),
            child: new Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(
                        left: 20, top: 10, right: 10, bottom: 10),
                    child: new Icon(Icons.lock)),
                new Container(
                  child: new Flexible(
                    child: new TextField(
                      obscureText: true,
                      onChanged: (text) {
                        newPass = text;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'New Password',
                      ),
                    ),
                  ), //flexible
                ), //container
              ], //widget
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
            // padding: const EdgeInsets.all(3.0),
            decoration: new BoxDecoration(
              border: new Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(
                const Radius.circular(30.0),
              ),
            ),
            child: new Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(
                        left: 20, top: 10, right: 10, bottom: 10),
                    child: new Icon(Icons.lock)),
                new Container(
                  child: new Flexible(
                    child: new TextField(
                      obscureText: true,
                      onChanged: (text) {
                        confirmPass = text;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Confirm Password',
                      ),
                    ),
                  ), //flexible
                ), //container
              ], //widget
            ),
          ),

          new GestureDetector(
              onTap: () {
                //print("Container clicked");
                submitButtonClick();
              },
              child: new Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                  // padding: const EdgeInsets.all(3.0),

                  decoration: new BoxDecoration(
                    color: Colors.pink,
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(30.0),
                    ),
                  ),
                  child: Center(
                      child: Text("Submit",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 18),
                          textDirection: TextDirection.ltr)))),

        ],
      ),
    );
  }



void submitButtonClick() {
     if (oldPass.trim().length == 0) {
      MyUtils.showtoast("Please enter old password");
    } else if (newPass.trim().length < 6) {
      MyUtils.showtoast("enter a password atleast 6 characters");
    } else if (confirmPass.trim().length < 6) {
       MyUtils.showtoast("enter a password atleast 6 characters");
     } else {
       callChangePasswordAPI();
    }
  }

  callChangePasswordAPI() {
    ProgressDialog progressDialog =
    new ProgressDialog(context, type: ProgressDialogType.Normal);
    progressDialog.update(
      progress: 50.0,
      message: "Please wait...",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()),
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
          color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
      messageTextStyle: TextStyle(
          color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600),
    );

    progressDialog.show();

    var req  ={
      "old_password": oldPass,
      "new_password": newPass,
      "confirm_password": confirmPass};
    var jsonReqString = json.encode(req);
    var apicall;
  if(prefs.getString('user_type')=='model'){
    apicall=  modelChangePasswordApi(jsonReqString,prefs.getString('token'));
  }else{
    apicall= partnerChangePasswordApi(jsonReqString,prefs.getString('token'));
  }
    apicall.then((response) {
      if (response.statusCode == 200) {
        progressDialog.hide();
        print(response.body);
        final jsonResponse = json.decode(response.body);
        print(jsonResponse);

        MyUtils.showtoast(jsonResponse['message'].toString());
        Navigator.pop(context);
      } else {
        progressDialog.hide();
        print(response.statusCode);
        MyUtils.showtoast("Something went wrong, please try after some time");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }

}
