import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:platform/platform.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:android_intent/android_intent.dart';
import 'package:url_launcher/url_launcher.dart';

class VenueScreen extends StatefulWidget {
  var distance,latitude,longitude;
  VenueScreen({this.distance,this.latitude,this.longitude}) ;

  @override
  _VenueScreenState createState() => _VenueScreenState();
}


SharedPreferences prefs;
Completer<GoogleMapController> _controller = Completer();

LatLng _latlng1 =LatLng(28.6221846, 77.3830408);
//static LatLng _latlng1 =  LatLng(28.6221846, 77.3830408);
//static LatLng _latlng1 =  LatLng(0.0, 0.0);
final Set<Marker> _markers = {};
LatLng _lastMapPosition;
class _VenueScreenState extends State<VenueScreen> {

  @override
  void initState() {
    _onAddMarkerSourceDestination();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: toolbarLayout("Venue"),
      body: BodyUI(),
    );
  }

  BodyUI() {
    return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
          child: Column(
            children: <Widget>[
              mapViewUI(),
              cardViewDistance(),

              Container(
                height: 50,
                margin: EdgeInsets.fromLTRB(20,20,20,10),
                child: RaisedButton(
                  onPressed: onStartNavigationClicked,
                    shape: RoundedRectangleBorder(
                    borderRadius:  BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),bottomLeft:Radius.circular(30.0),bottomRight: Radius.circular(30.0) )),
                    child: Align(alignment: Alignment.center,
                      child: Text("Start Navigation",
                          style: TextStyle(color: Colors.white,fontSize: 15 )),
                    ),
                    color:  Colors.pink,
                    disabledColor: Colors.pink),
              )
            ],
          ),));
  }

  cardViewDistance(){
    return  Card(
      elevation: 3,
      margin: EdgeInsets.fromLTRB(15,10,15,10),
      child: Container(
          child: Row(
            //mainAxisSize: MainAxisSize.max,
            //mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10),
              ),
              Flexible(
                child: Container(
                 // color: Colors.black,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,

                    children: <Widget>[
                       Row(
                         mainAxisSize: MainAxisSize.max,
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: <Widget>[
                          Row(
                             mainAxisSize: MainAxisSize.max,
                             children: <Widget>[
                             Container(
                               width:24,
                               height: 24,
                                 child: Align(alignment: Alignment.topLeft,
                                     child: Icon(Icons.watch,)),alignment: Alignment.topLeft,),
                               SizedBox(width: 5,),


                               Text("Distance",
                                  style: TextStyle(fontSize: 15,color: Colors.pink,fontWeight: FontWeight.bold)),

                         ]),
                           Align(alignment:Alignment.topRight,child: Icon(Icons.map,)),
                         ],
                       ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(width: 30,height: 30,),


                            Text(widget.distance+" KM",
                                style: TextStyle(fontSize: 15,color: Colors.black,fontWeight: FontWeight.bold)),
                          ]),
                      SizedBox(height: 10),

                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }


  mapViewUI() {
    return Container(
      height: 300,
      child: GoogleMap(
        //onTap: _onAddMarker(),
        onMapCreated: _onMapCreated,
        initialCameraPosition: CameraPosition(
          target: _latlng1,
          zoom: 11.0,
        ),
        mapType: MapType.normal,
        markers: _markers,
        onCameraMove: _onCameraMove,
        myLocationButtonEnabled: true,
        myLocationEnabled: true,

      ),
    );
  }


  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  void _onAddMarkerSourceDestination() {

    Marker resultMarker = Marker(
      markerId: MarkerId("You"),
      infoWindow: InfoWindow(
          title: "You",
          snippet: "Harimohan"),
      position: LatLng(MyConstants.currentLat,MyConstants.currentLong),
    );
    _markers.add(resultMarker);

    Marker resultMarker1 = Marker(
      markerId: MarkerId("destination"),
      infoWindow: InfoWindow(
          title: "destination",
          //snippet: "Harimohan"
      ),
      position: LatLng(double.parse(widget.latitude),double.parse(widget.longitude)),
    );
    _markers.add(resultMarker1);

  }

  onStartNavigationClicked() async {
   // String origin=MyConstants.currentLat.toString()+","+MyConstants.currentLong.toString();  // lat,long like 123.34,68.56
    String origin=""+MyConstants.currentLat.toString()+","+MyConstants.currentLong.toString();  // lat,long like 123.34,68.56

    print(origin);
    String destination=widget.latitude+","+widget.longitude;
    if (new LocalPlatform().isAndroid) {
      final AndroidIntent intent = new AndroidIntent(
          action: 'action_view',
          data: Uri.encodeFull(
              "https://www.google.com/maps/dir/?api=1&origin=" +
                  origin + "&destination=" + destination + "&travelmode=driving&dir_action=navigate"),
          package: 'com.google.android.apps.maps');
      intent.launch();
    }
    else {
      String url = "https://www.google.com/maps/dir/?api=1&origin=" + origin + "&destination=" + destination + "&travelmode=driving&dir_action=navigate";
      if (await canLaunch(url)) {
    await launch(url);
    } else {
    throw 'Could not launch $url';
    }
  }
  }
}
