import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/Screens/AboutScreen.dart';
import 'package:flutter_demo/Screens/ChangePasswordScreen.dart';
import 'package:flutter_demo/Screens/ContactUsScreen.dart';
import 'package:flutter_demo/Screens/ReservedofferScreen.dart';
import 'package:flutter_demo/Screens/TermsOfServicesScreen.dart';
import 'package:flutter_demo/bottombarScreens/AudioPlayerScreen.dart';
import 'package:flutter_demo/bottombarScreens/VideoPlayerScreen.dart';
import 'package:flutter_demo/bottombarScreens/PaymentGatewaysScreens.dart';
import 'package:flutter_demo/bottombarScreens/PlayoutExample.dart';
import 'package:flutter_demo/bottombarScreens/QrcodeScennerScreen.dart';
import 'package:flutter_demo/bottombarScreens/VanityMainScreen.dart';
import 'package:flutter_demo/model/PartnerEventList_model.dart';
import 'package:flutter_demo/util/MyUIs.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

import 'LoginScreen.dart';
import 'MapViewScreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'MyProfileScreen.dart';
import 'PrivacyPolicyScreen.dart';
import 'SubPage.dart';

class HomeScreen extends StatefulWidget {
  String token;

  HomeScreen({this.token});

  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  List<Datum> eventList = List<Datum>();
  SharedPreferences prefs;
  int selectedIndex = 0;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    /* Future.delayed(Duration(seconds: 2), () {
      this. callEventListApi(widget.token);
    });*/
    sharedPrefrenceInit();

    //for firebase fcm
    firebaseCloudMessaging_Listeners();
  }

  sharedPrefrenceInit() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<List<Datum>> _fetchData() async {
    var BASE_URL = "https://admin.yourwuber.com/api/";
    final response = await http.get(
      '$BASE_URL/auth/services',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY':
            "JDJ5JDEwJEJjd3NaamtFMWQzZUk1QzAwVUE1T3VlZWViWndFb00uQ1RSc3RodVJsQXFzWGtENGNhM01H",
        //'APPTOKEN' : apptoken
      },
    );

    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      print(items);
      List<Datum> listOfUsers = items.map<Datum>((json) {
        return Datum.fromJson(json);
      }).toList();

      return listOfUsers;
    } else {
      throw Exception('Failed to load internet');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('HomeScreen'),
          backgroundColor: Colors.pink,
        ),
        resizeToAvoidBottomPadding: false,
        drawer: Drawer(
            child: Container(
              width: double.infinity,
                height: double.infinity,
                decoration: new BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: new ExactAssetImage(
                        'assets/images/navigation_menu_bg.png'),
                  ),
                ),
                child: navigationDrawer())),
        bottomNavigationBar: bottomNavigation(),
        body: tabListner(selectedIndex)); /* bodyUI_withApi()*/
  }

  tabListner(index) {
    print(index);
    switch (index) {
      case 0:
        {
          return VanityMainScreen();
        }
      case 1:
        {
          return QrcodeScennerScreen();
        }
      case 2:
        {
          return PaymentGatewaysScreens();
        }
      case 3:
        {
          return MediaPlayerScreen();
        }
      case 4:
        {
          return PlayoutExample();
        }
      case 5:
        {
          return AudioAssets();
        }
    }
  }

  bottomNavigation() {
    return BottomNavigationBar(
      currentIndex: selectedIndex,
      // backgroundColor: Colors.white,
      fixedColor: Colors.green,
      onTap: onItemTapped,
      type: BottomNavigationBarType.fixed,
      items: [
        BottomNavigationBarItem(
            icon: SizedBox(
              child: new IconButton(
                icon: new Image.asset(
                  "assets/images/current_jobs.png",
                  color: Colors.grey,
                ),
              ),
              width: 35,
              height: 35,
            ),
            activeIcon: SizedBox(
              child: new IconButton(
                icon: new Image.asset(
                  "assets/images/current_jobs.png",
                  color: Colors.green,
                ),
              ),
              width: 35,
              height: 35,
            ),
            title: Text("Home", textAlign: TextAlign.center)),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.image,
            ),
            title: Text("Qrcode Scanner", textAlign: TextAlign.center)),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.done,
            ),
            title: Text(
              "Payment Gateway",
              textAlign: TextAlign.center,
            )),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.done,
            ),
            title: Text("Media Player")),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.done,
            ),
            title: Text("Flutter Playout")),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.done,
            ),
            title: Text("More")),
      ],
    );
  }

  Widget navigationDrawer() {
    return ListView(
      padding: EdgeInsets.all(0.0),
      children: <Widget>[
        DrawerHeader(
          child: new Container(
            margin: EdgeInsets.only(bottom: 0.0),
            padding: EdgeInsets.only(bottom: 0.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  radius: 40.0,
                  backgroundImage: AssetImage("assets/images/profile_girl.png"),
                  backgroundColor: Colors.transparent,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "John Cyle",
                )
              ],
            ),
          ),
          /*decoration: new BoxDecoration(
     color: Colors.orange
    ),*/
        ),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              onItemTapped(0);
            },
            child: drawerItem("Home")),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => MyProfileScreen()));
            },
            child: drawerItem("Profile")),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => ReservedofferScreen()));            },
            child: drawerItem("Reserved Offers")),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => ChangePasswordScreen()));
            },
            child: drawerItem("Change Password")),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => ContactUsScreen()));
            },
            child: drawerItem("Contact Us")),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => PrivacyPolicyScreen()));
              },
            child: drawerItem("Privacy Policy")),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              //onItemTapped(1);
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => AboutScreen()));


            },
            child: drawerItem("About US")),
        GestureDetector(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => TermsOfServicesScreen()));
            },
            child: drawerItem("Terms of Services")),


        GestureDetector(
          onTap: (){
            Navigator.pop(context);
            prefs.clear();
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (ctxt) => LoginScreen()));
          },
          child: Container(
            height: 45,
            margin: const EdgeInsets.only(left: 25, top: 15, right: 25),
            // padding: const EdgeInsets.all(3.0),
            decoration: new BoxDecoration(
              color: Colors.transparent,
              border: new Border.all(color: Colors.pink),
              borderRadius: const BorderRadius.all(
                const Radius.circular(30.0),
              ),
            ),
            child:  Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(
                        left: 12, top: 10, right: 10, bottom: 10),
                    child:  Icon(Icons.label_outline,color: Colors.pink,)),
                Container(
                  child:  Flexible(
                    child: Text("Logout",style: TextStyle(/*fontWeight: FontWeight.bold,*/color: Colors.pink,fontSize: 16),),
                  ), //flexible
                ), //container
              ], //widget
            ),),
        )
        /* ListTile(
          title: new Text("Home"),
          leading: Icon(Icons.home),
          onTap: () {
            Navigator.pop(context);
            onItemTapped(1);
            /*Navigator.push(ctxt,
     MaterialPageRoute(builder: (ctxt) => new FirstPage()));*/
          },
        ),
        ListTile(
          title: new Text("About"),
          leading: Icon(Icons.account_box),
          onTap: () {
            Navigator.pop(context);
            onItemTapped(3);
            */ /*Navigator.push(ctxt,
    new MaterialPageRoute(builder: (ctxt) => new SecondPage()));*/ /*
          },
        ),
        ListTile(
          title: new Text("Logout"),
          leading: Icon(Icons.skip_previous),
          onTap: () {
            Navigator.pop(context);
            prefs.clear();
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (ctxt) => LoginScreen()));
          },
        ),*/
      ],
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  void firebaseCloudMessaging_Listeners() {
    _firebaseMessaging.subscribeToTopic('flutter');
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token) {
      print("token" + token);
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        var _message;
        if (Theme.of(context).platform == TargetPlatform.iOS) {
          _message = message['aps']['alert'];
        } else {
          _message = message;
        }

        showDialogForNotificiation_forground(_message);
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
        // if(message['notification']['title']=="test"){
        Navigator.of(context).pushReplacement(new MaterialPageRoute(
            builder: (BuildContext context) => SubPage()));
        //  }
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  showDialogForNotificiation_forground(message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['title']),
              subtitle: Text(message['body']),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.amber,
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
    );
  }
}
