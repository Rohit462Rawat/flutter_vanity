import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/model/LoginResponse_model.dart';
import 'package:flutter_demo/model/login_model.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ForgotPasswordScreen.dart';
import 'HomeScreen.dart';
import 'SignUpScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  SharedPreferences prefs;
  String email, password;
  final TextEditingController _emailFilter = new TextEditingController();

  @override
  void initState() {
    super.initState();
    addStringToSF();

  }

  addStringToSF() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: double.infinity,
            decoration: new BoxDecoration(
              image: DecorationImage(
                  image: new ExactAssetImage('assets/images/login_bg.png'),
                  fit: BoxFit.fill),
            ),
            child: SingleChildScrollView(child: _buildUIComposer())));
  }

  Widget _buildUIComposer() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              height: 150,
              margin: const EdgeInsets.only(left: 20, top: 80, right: 20),
              // padding: const EdgeInsets.all(3.0),

              child: new Image.asset('assets/images/app_icon.png')),
          Container(
            height: 50,
            margin: const EdgeInsets.only(left: 20, top: 50, right: 20),
            // padding: const EdgeInsets.all(3.0),
            decoration: new BoxDecoration(
              border: new Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(
                const Radius.circular(30.0),
              ),
            ),
            child: new Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(
                        left: 20, top: 10, right: 10, bottom: 10),
                    child: new Icon(Icons.person)),
                new Container(
                  child: new Flexible(
                    child: new TextField(
                      onChanged: (text) {

                        email = text;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Email',
                      ),
                    ),
                  ), //flexible
                ), //container
              ], //widget
            ),
          ),
          Container(
            height: 50,
            margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
            // padding: const EdgeInsets.all(3.0),
            decoration: new BoxDecoration(
              border: new Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(
                const Radius.circular(30.0),
              ),
            ),
            child: new Row(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(
                        left: 20, top: 10, right: 10, bottom: 10),
                    child: new Icon(Icons.lock)),
                new Container(
                  child: new Flexible(
                    child: new TextField(
                      obscureText: true,
                      onChanged: (text) {
                        password = text;
                      },
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Password',
                      ),
                    ),
                  ), //flexible
                ), //container
              ], //widget
            ),
          ),
          Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 20, right: 10),
              // padding: const EdgeInsets.all(3.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    onPressed: forgotPasswordButtonClick,
                      child: Text("Forgot Password?",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontStyle: FontStyle.normal)))
                ],
              )),
          new GestureDetector(
              onTap: () {
                //print("Container clicked");
                submitButtonClick();
              },
              child: new Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                  // padding: const EdgeInsets.all(3.0),

                  decoration: new BoxDecoration(
                    color: Colors.pink,
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(30.0),
                    ),
                  ),
                  child: Center(
                      child: Text("Submit",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 18),
                          textDirection: TextDirection.ltr)))),
          Container(
              height: 50,
              width: double.infinity,
              margin: const EdgeInsets.only(
                  left: 20, top: 20, right: 20, bottom: 20),
              // padding: const EdgeInsets.all(3.0),

              decoration: new BoxDecoration(
                color: Colors.pink,
                borderRadius: const BorderRadius.all(
                  const Radius.circular(30.0),
                ),
              ),
              child: FlatButton(
                onPressed: applyNowButtonClick,
                  child: Text("Apply Now",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontStyle: FontStyle.normal))))
        ],
      ),
    );
  }


  applyNowButtonClick(){
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => SignUpScreen()));
  }

  forgotPasswordButtonClick(){
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => ForgotPasswordScreen()));
  }
  void submitButtonClick() {
    bool emailValid =
        RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    if (!emailValid) {
      MyUtils.showtoast("Please enter valid email");
    } else if (password.trim().length == 0) {
      MyUtils.showtoast("Please enter password");
    } else if (password.trim().length < 6) {
      MyUtils.showtoast("enter a password atleast 6 characters");
    } else {
      callLoginAPI1(email, password);
      /* Login loginReq = Login(
          email: email,
          password:password,
          device:'Android',
          deviceId:'dsfsdf45566',
          fcmKey:'fsdgdfhbdfhdhfdhdfhf'
      );

      FutureBuilder<LoginResponse>(
        future: createLoginRequestApi(loginReq),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // print('Name: ${snapshot.data.data}');
            // print('Email: ${snapshot.data.data.email}');
            return Text(snapshot.data.data.email.toString());

          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }

          // By default, show a loading spinner.
          return CircularProgressIndicator();
        },
      );*/
    }
  }

  callLoginAPI1(String email, String password) {
    ProgressDialog progressDialog =
        new ProgressDialog(context, type: ProgressDialogType.Normal);
   // progressDialog.setMessage("Please wait…");
    progressDialog.update(
      progress: 50.0,
      message: "Please wait...",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()),
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
          color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
      messageTextStyle: TextStyle(
          color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600),
    );

    progressDialog.show();

    Login loginReq = Login(
        email: email,
        password: password,
        device: 'Android',
        deviceId: 'dsfsdf45566',
        fcmKey: 'fsdgdfhbdfhdhfdhdfhf');
    createLoginRequestApi1(loginReq).then((response) {
      if (response.statusCode == 200) {
        progressDialog.hide();
        print(response.body);
        final jsonResponse = json.decode(response.body);
        LoginResponse loginResponse = new LoginResponse.fromJson(jsonResponse);

        //setting token into shared prefrences
        prefs.setString('token', loginResponse.data.token);
        prefs.setString('user_type', loginResponse.data.groupName);

        MyUtils.showtoast(loginResponse.message);
        // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
        Navigator.of(context).pushReplacement(new MaterialPageRoute(
            builder: (BuildContext context) => HomeScreen(
                  token: loginResponse.data.token,
                )));
      } else {
        progressDialog.hide();
        print(response.statusCode);
        MyUtils.showtoast("Something went wrong, please try after some time");
      }
    }).catchError((error) {
      print('error : $error');
    });
  }
}
