// To parse this JSON data, do
//
//     final login = loginFromJson(jsonString);

import 'dart:convert';

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());

class Login {
  String email;
  String password;
  String device;
  String deviceId;
  String fcmKey;

  Login({
    this.email,
    this.password,
    this.device,
    this.deviceId,
    this.fcmKey,
  });

  factory Login.fromJson(Map<String, dynamic> json) => new Login(
    email: json["email"],
    password: json["password"],
    device: json["device"],
    deviceId: json["device_id"],
    fcmKey: json["fcm_key"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
    "device": device,
    "device_id": deviceId,
    "fcm_key": fcmKey,
  };
}
