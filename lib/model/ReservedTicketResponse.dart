// To parse this JSON data, do
//
//     final reservedTicketResponse = reservedTicketResponseFromJson(jsonString);

import 'dart:convert';

ReservedTicketResponse reservedTicketResponseFromJson(String str) => ReservedTicketResponse.fromJson(json.decode(str));

String reservedTicketResponseToJson(ReservedTicketResponse data) => json.encode(data.toJson());

class ReservedTicketResponse {
  String success;
  String title;
  String message;
  List<ReservedTicketData> data;

  ReservedTicketResponse({
    this.success,
    this.title,
    this.message,
    this.data,
  });

  factory ReservedTicketResponse.fromJson(Map<String, dynamic> json) => ReservedTicketResponse(
    success: json["success"],
    title: json["title"],
    message: json["message"],
    data: List<ReservedTicketData>.from(json["data"].map((x) => ReservedTicketData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "title": title,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class ReservedTicketData {
  String id;
  String ticketId;
  String eventId;
  String eventOfferId;
  String active;
  String status;
  dynamic qrCode;
  String reserveBy;
  DateTime createdAt;
  DateTime updatedAt;
  DateTime expireDate;
  String offerName;
  String offerDescription;
  String isPremium;
  dynamic userFirstName;
  dynamic userLastName;
  dynamic userEmail;
  String eventName;
  String organiserName;
  String qrCodePng;

  ReservedTicketData({
    this.id,
    this.ticketId,
    this.eventId,
    this.eventOfferId,
    this.active,
    this.status,
    this.qrCode,
    this.reserveBy,
    this.createdAt,
    this.updatedAt,
    this.expireDate,
    this.offerName,
    this.offerDescription,
    this.isPremium,
    this.userFirstName,
    this.userLastName,
    this.userEmail,
    this.eventName,
    this.organiserName,
    this.qrCodePng,
  });

  factory ReservedTicketData.fromJson(Map<String, dynamic> json) => ReservedTicketData(
    id: json["id"],
    ticketId: json["ticket_id"],
    eventId: json["event_id"],
    eventOfferId: json["event_offer_id"],
    active: json["active"],
    status: json["status"],
    qrCode: json["qr_code"],
    reserveBy: json["reserve_by"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    expireDate: DateTime.parse(json["expire_date"]),
    offerName: json["offer_name"],
    offerDescription: json["offer_description"],
    isPremium: json["is_premium"],
    userFirstName: json["user_first_name"],
    userLastName: json["user_last_name"],
    userEmail: json["user_email"],
    eventName: json["event_name"],
    organiserName: json["organiser_name"],
    qrCodePng: json["qr_code_png"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "ticket_id": ticketId,
    "event_id": eventId,
    "event_offer_id": eventOfferId,
    "active": active,
    "status": status,
    "qr_code": qrCode,
    "reserve_by": reserveBy,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "expire_date": expireDate.toIso8601String(),
    "offer_name": offerName,
    "offer_description": offerDescription,
    "is_premium": isPremium,
    "user_first_name": userFirstName,
    "user_last_name": userLastName,
    "user_email": userEmail,
    "event_name": eventName,
    "organiser_name": organiserName,
    "qr_code_png": qrCodePng,
  };
}
