// To parse this JSON data, do
//
//     final eventListRequest = eventListRequestFromJson(jsonString);

import 'dart:convert';

EventListRequest eventListRequestFromJson(String str) => EventListRequest.fromJson(json.decode(str));

String eventListRequestToJson(EventListRequest data) => json.encode(data.toJson());

class EventListRequest {
  String latitude;
  String longitude;
  String radius;
  String unitType;
  String timezone;

  EventListRequest({
    this.latitude,
    this.longitude,
    this.radius,
    this.unitType,
    this.timezone,
  });

  factory EventListRequest.fromJson(Map<String, dynamic> json) => EventListRequest(
    latitude: json["latitude"],
    longitude: json["longitude"],
    radius: json["radius"],
    unitType: json["unit_type"],
    timezone: json["timezone"],
  );

  Map<String, dynamic> toJson() => {
    "latitude": latitude,
    "longitude": longitude,
    "radius": radius,
    "unit_type": unitType,
    "timezone": timezone,
  };
}
