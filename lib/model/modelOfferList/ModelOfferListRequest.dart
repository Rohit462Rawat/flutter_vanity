// To parse this JSON data, do
//
//     final modelOfferListRequest = modelOfferListRequestFromJson(jsonString);

import 'dart:convert';

ModelOfferListRequest modelOfferListRequestFromJson(String str) => ModelOfferListRequest.fromJson(json.decode(str));

String modelOfferListRequestToJson(ModelOfferListRequest data) => json.encode(data.toJson());

class ModelOfferListRequest {
  int eventId;
  String page;
  String limit;
  String timezone;

  ModelOfferListRequest({
    this.eventId,
    this.page,
    this.limit,
    this.timezone,
  });

  factory ModelOfferListRequest.fromJson(Map<String, dynamic> json) => ModelOfferListRequest(
    eventId: json["event_id"],
    page: json["page"],
    limit: json["limit"],
    timezone: json["timezone"],
  );

  Map<String, dynamic> toJson() => {
    "event_id": eventId,
    "page": page,
    "limit": limit,
    "timezone": timezone,
  };
}
