// To parse this JSON data, do
//
//     final modelOfferListResponse = modelOfferListResponseFromJson(jsonString);

import 'dart:convert';

ModelOfferListResponse modelOfferListResponseFromJson(String str) => ModelOfferListResponse.fromJson(json.decode(str));

String modelOfferListResponseToJson(ModelOfferListResponse data) => json.encode(data.toJson());

class ModelOfferListResponse {
  String success;
  String title;
  String message;
  List<OfferListData> data;
  List<dynamic> error;
  int statusCode;

  ModelOfferListResponse({
    this.success,
    this.title,
    this.message,
    this.data,
    this.error,
    this.statusCode,
  });

  factory ModelOfferListResponse.fromJson(Map<String, dynamic> json) => ModelOfferListResponse(
    success: json["success"],
    title: json["title"],
    message: json["message"],
    data: List<OfferListData>.from(json["data"].map((x) => OfferListData.fromJson(x))),
    error: List<dynamic>.from(json["error"].map((x) => x)),
    statusCode: json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "title": title,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "error": List<dynamic>.from(error.map((x) => x)),
    "status_code": statusCode,
  };
}

class OfferListData {
  String id;
  String eventId;
  String userId;
  String name;
  String description;
  String frequency;
  String thumbnail;
  DateTime openingDatetime;
  DateTime closingDatetime;
  String active;
  DateTime createdAt;
  DateTime updatedAt;
  String isPremium;
  String ticketAvailable;
  DateTime offerExpireDate;

  OfferListData({
    this.id,
    this.eventId,
    this.userId,
    this.name,
    this.description,
    this.frequency,
    this.thumbnail,
    this.openingDatetime,
    this.closingDatetime,
    this.active,
    this.createdAt,
    this.updatedAt,
    this.isPremium,
    this.ticketAvailable,
    this.offerExpireDate,
  });

  factory OfferListData.fromJson(Map<String, dynamic> json) => OfferListData(
    id: json["id"],
    eventId: json["event_id"],
    userId: json["user_id"],
    name: json["name"],
    description: json["description"],
    frequency: json["frequency"],
    thumbnail: json["thumbnail"],
    openingDatetime: DateTime.parse(json["opening_datetime"]),
    closingDatetime: DateTime.parse(json["closing_datetime"]),
    active: json["active"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    isPremium: json["is_premium"],
    ticketAvailable: json["ticket_available"],
    offerExpireDate: DateTime.parse(json["offer_expire_date"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "event_id": eventId,
    "user_id": userId,
    "name": name,
    "description": description,
    "frequency": frequency,
    "thumbnail": thumbnail,
    "opening_datetime": openingDatetime.toIso8601String(),
    "closing_datetime": closingDatetime.toIso8601String(),
    "active": active,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "is_premium": isPremium,
    "ticket_available": ticketAvailable,
    "offer_expire_date": offerExpireDate.toIso8601String(),
  };
}
