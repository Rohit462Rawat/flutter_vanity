// To parse this JSON data, do
//
//     final modelTimeAvailabilityResponse = modelTimeAvailabilityResponseFromJson(jsonString);

import 'dart:convert';

ModelTimeAvailabilityResponse modelTimeAvailabilityResponseFromJson(String str) => ModelTimeAvailabilityResponse.fromJson(json.decode(str));

String modelTimeAvailabilityResponseToJson(ModelTimeAvailabilityResponse data) => json.encode(data.toJson());

class ModelTimeAvailabilityResponse {
  String success;
  String title;
  String message;
  List<TimeAvailabiltyData> data;
  List<dynamic> error;
  int statusCode;

  ModelTimeAvailabilityResponse({
    this.success,
    this.title,
    this.message,
    this.data,
    this.error,
    this.statusCode,
  });

  factory ModelTimeAvailabilityResponse.fromJson(Map<String, dynamic> json) => ModelTimeAvailabilityResponse(
    success: json["success"],
    title: json["title"],
    message: json["message"],
    data: List<TimeAvailabiltyData>.from(json["data"].map((x) => TimeAvailabiltyData.fromJson(x))),
    error: List<dynamic>.from(json["error"].map((x) => x)),
    statusCode: json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "title": title,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "error": List<dynamic>.from(error.map((x) => x)),
    "status_code": statusCode,
  };
}

class TimeAvailabiltyData {
  String id;
  String eventId;
  String offerId;
  String day;
  String openingTime;
  String closingTime;
  DateTime createdAt;
  DateTime updatedAt;

  TimeAvailabiltyData({
    this.id,
    this.eventId,
    this.offerId,
    this.day,
    this.openingTime,
    this.closingTime,
    this.createdAt,
    this.updatedAt,
  });

  factory TimeAvailabiltyData.fromJson(Map<String, dynamic> json) => TimeAvailabiltyData(
    id: json["id"],
    eventId: json["event_id"],
    offerId: json["offer_id"],
    day: json["day"],
    openingTime: json["opening_time"],
    closingTime: json["closing_time"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "event_id": eventId,
    "offer_id": offerId,
    "day": day,
    "opening_time": openingTime,
    "closing_time": closingTime,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
