// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  String success;
  String title;
  String message;
  Data data;
  List<dynamic> error;
  int statusCode;

  LoginResponse({
    this.success,
    this.title,
    this.message,
    this.data,
    this.error,
    this.statusCode,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    success: json["success"],
    title: json["title"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
    statusCode: json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "title": title,
    "message": message,
    "data": data.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
    "status_code": statusCode,
  };
}

class Data {
  String id;
  String ipAddress;
  String username;
  String password;
  dynamic salt;
  String email;
  dynamic rememberCode;
  String createdOn;
  String lastLogin;
  String active;
  String firstName;
  String lastName;
  dynamic company;
  String phone;
  String profilePic;
  String isOtpVerify;
  String groupId;
  String groupName;
  String token;

  Data({
    this.id,
    this.ipAddress,
    this.username,
    this.password,
    this.salt,
    this.email,
    this.rememberCode,
    this.createdOn,
    this.lastLogin,
    this.active,
    this.firstName,
    this.lastName,
    this.company,
    this.phone,
    this.profilePic,
    this.isOtpVerify,
    this.groupId,
    this.groupName,
    this.token,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    ipAddress: json["ip_address"],
    username: json["username"],
    password: json["password"],
    salt: json["salt"],
    email: json["email"],
    rememberCode: json["remember_code"],
    createdOn: json["created_on"],
    lastLogin: json["last_login"],
    active: json["active"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    company: json["company"],
    phone: json["phone"],
    profilePic: json["profile_pic"],
    isOtpVerify: json["is_otp_verify"],
    groupId: json["group_id"],
    groupName: json["group_name"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "ip_address": ipAddress,
    "username": username,
    "password": password,
    "salt": salt,
    "email": email,
    "remember_code": rememberCode,
    "created_on": createdOn,
    "last_login": lastLogin,
    "active": active,
    "first_name": firstName,
    "last_name": lastName,
    "company": company,
    "phone": phone,
    "profile_pic": profilePic,
    "is_otp_verify": isOtpVerify,
    "group_id": groupId,
    "group_name": groupName,
    "token": token,
  };
}
