// To parse this JSON data, do
//
//     final modelProfileResponse = modelProfileResponseFromJson(jsonString);

import 'dart:convert';

ModelProfileResponse modelProfileResponseFromJson(String str) => ModelProfileResponse.fromJson(json.decode(str));

String modelProfileResponseToJson(ModelProfileResponse data) => json.encode(data.toJson());

class ModelProfileResponse {
  String success;
  String title;
  String message;
  ModelProfileData data;

  ModelProfileResponse({
    this.success,
    this.title,
    this.message,
    this.data,
  });

  factory ModelProfileResponse.fromJson(Map<String, dynamic> json) => ModelProfileResponse(
    success: json["success"],
    title: json["title"],
    message: json["message"],
    data: ModelProfileData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "title": title,
    "message": message,
    "data": data.toJson(),
  };
}

class ModelProfileData {
  String id;
  String ipAddress;
  String username;
  dynamic salt;
  String email;
  dynamic activationCode;
  String forgottenPasswordCode;
  String forgottenPasswordTime;
  dynamic rememberCode;
  String createdOn;
  String lastLogin;
  String active;
  String firstName;
  String lastName;
  dynamic company;
  String phone;
  String profilePic;
  dynamic website;
  DateTime dateOfBirth;
  String address;
  String city;
  dynamic state;
  String country;
  String currentAgency;
  String motherAgency;
  dynamic socialGoogleLink;
  dynamic socialFacebookLink;
  dynamic socialTwitterLink;
  String socialInstagramLink;
  DateTime updatedAt;
  dynamic otp;
  String isOtpVerify;
  String device;
  String deviceId;
  String fcmKey;
  String compositeCardFrontSide;
  String compositeCardBackSide;
  List<String> polaroids;
  dynamic customMessage;
  String interest;
  String isMailSent;

  ModelProfileData({
    this.id,
    this.ipAddress,
    this.username,
    this.salt,
    this.email,
    this.activationCode,
    this.forgottenPasswordCode,
    this.forgottenPasswordTime,
    this.rememberCode,
    this.createdOn,
    this.lastLogin,
    this.active,
    this.firstName,
    this.lastName,
    this.company,
    this.phone,
    this.profilePic,
    this.website,
    this.dateOfBirth,
    this.address,
    this.city,
    this.state,
    this.country,
    this.currentAgency,
    this.motherAgency,
    this.socialGoogleLink,
    this.socialFacebookLink,
    this.socialTwitterLink,
    this.socialInstagramLink,
    this.updatedAt,
    this.otp,
    this.isOtpVerify,
    this.device,
    this.deviceId,
    this.fcmKey,
    this.compositeCardFrontSide,
    this.compositeCardBackSide,
    this.polaroids,
    this.customMessage,
    this.interest,
    this.isMailSent,
  });

  factory ModelProfileData.fromJson(Map<String, dynamic> json) => ModelProfileData(
    id: json["id"],
    ipAddress: json["ip_address"],
    username: json["username"],
    salt: json["salt"],
    email: json["email"],
    activationCode: json["activation_code"],
    forgottenPasswordCode: json["forgotten_password_code"],
    forgottenPasswordTime: json["forgotten_password_time"],
    rememberCode: json["remember_code"],
    createdOn: json["created_on"],
    lastLogin: json["last_login"],
    active: json["active"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    company: json["company"],
    phone: json["phone"],
    profilePic: json["profile_pic"],
    website: json["website"],
    dateOfBirth: DateTime.parse(json["date_of_birth"]),
    address: json["address"],
    city: json["city"],
    state: json["state"],
    country: json["country"],
    currentAgency: json["current_agency"],
    motherAgency: json["mother_agency"],
    socialGoogleLink: json["social_google_link"],
    socialFacebookLink: json["social_facebook_link"],
    socialTwitterLink: json["social_twitter_link"],
    socialInstagramLink: json["social_instagram_link"],
    updatedAt: DateTime.parse(json["updated_at"]),
    otp: json["otp"],
    isOtpVerify: json["is_otp_verify"],
    device: json["device"],
    deviceId: json["device_id"],
    fcmKey: json["fcm_key"],
    compositeCardFrontSide: json["composite_card_front_side"],
    compositeCardBackSide: json["composite_card_back_side"],
    polaroids: List<String>.from(json["polaroids"].map((x) => x)),
    customMessage: json["custom_message"],
    interest: json["interest"],
    isMailSent: json["is_mail_sent"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "ip_address": ipAddress,
    "username": username,
    "salt": salt,
    "email": email,
    "activation_code": activationCode,
    "forgotten_password_code": forgottenPasswordCode,
    "forgotten_password_time": forgottenPasswordTime,
    "remember_code": rememberCode,
    "created_on": createdOn,
    "last_login": lastLogin,
    "active": active,
    "first_name": firstName,
    "last_name": lastName,
    "company": company,
    "phone": phone,
    "profile_pic": profilePic,
    "website": website,
    "date_of_birth": "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
    "address": address,
    "city": city,
    "state": state,
    "country": country,
    "current_agency": currentAgency,
    "mother_agency": motherAgency,
    "social_google_link": socialGoogleLink,
    "social_facebook_link": socialFacebookLink,
    "social_twitter_link": socialTwitterLink,
    "social_instagram_link": socialInstagramLink,
    "updated_at": updatedAt.toIso8601String(),
    "otp": otp,
    "is_otp_verify": isOtpVerify,
    "device": device,
    "device_id": deviceId,
    "fcm_key": fcmKey,
    "composite_card_front_side": compositeCardFrontSide,
    "composite_card_back_side": compositeCardBackSide,
    "polaroids": List<dynamic>.from(polaroids.map((x) => x)),
    "custom_message": customMessage,
    "interest": interest,
    "is_mail_sent": isMailSent,
  };
}
