// To parse this JSON data, do
//
//     final partnerEventList = partnerEventListFromJson(jsonString);

import 'dart:convert';

PartnerEventList partnerEventListFromJson(String str) => PartnerEventList.fromJson(json.decode(str));

String partnerEventListToJson(PartnerEventList data) => json.encode(data.toJson());

class PartnerEventList {
  String success;
  String title;
  String message;
  List<Datum> data;

  PartnerEventList({
    this.success,
    this.title,
    this.message,
    this.data,
  });

  factory PartnerEventList.fromJson(Map<String, dynamic> json) => PartnerEventList(
    success: json["success"],
    title: json["title"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "title": title,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  String id;
  String organiserName;
  dynamic organiserLogo;
  String name;
  String description;
  String userId;
  String address;
  String latitude;
  String longitude;
  String phone1;
  dynamic phone2;
  String city;
  dynamic pincode;
  String state;
  String country;
  DateTime openingTime;
  DateTime closingTime;
  dynamic thumbnail;
  List<dynamic> galleries;
  String active;
  String email;
  String firstName;
  String lastName;
  String ticketAvailable;
  String offerAvailable;
  String distance;

  Datum({
    this.id,
    this.organiserName,
    this.organiserLogo,
    this.name,
    this.description,
    this.userId,
    this.address,
    this.latitude,
    this.longitude,
    this.phone1,
    this.phone2,
    this.city,
    this.pincode,
    this.state,
    this.country,
    this.openingTime,
    this.closingTime,
    this.thumbnail,
    this.galleries,
    this.active,
    this.email,
    this.firstName,
    this.lastName,
    this.ticketAvailable,
    this.offerAvailable,
    this.distance,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    organiserName: json["organiser_name"],
    organiserLogo: json["organiser_logo"],
    name: json["name"],
    description: json["description"],
    userId: json["user_id"],
    address: json["address"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    phone1: json["phone1"],
    phone2: json["phone2"],
    city: json["city"],
    pincode: json["pincode"],
    state: json["state"],
    country: json["country"],
    openingTime: DateTime.parse(json["opening_time"]),
    closingTime: DateTime.parse(json["closing_time"]),
    thumbnail: json["thumbnail"],
    galleries: List<dynamic>.from(json["galleries"].map((x) => x)),
    active: json["active"],
    email: json["email"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    ticketAvailable: json["ticket_available"],
    offerAvailable: json["offer_available"],
    distance: json["distance"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "organiser_name": organiserName,
    "organiser_logo": organiserLogo,
    "name": name,
    "description": description,
    "user_id": userId,
    "address": address,
    "latitude": latitude,
    "longitude": longitude,
    "phone1": phone1,
    "phone2": phone2,
    "city": city,
    "pincode": pincode,
    "state": state,
    "country": country,
    "opening_time": openingTime.toIso8601String(),
    "closing_time": closingTime.toIso8601String(),
    "thumbnail": thumbnail,
    "galleries": List<dynamic>.from(galleries.map((x) => x)),
    "active": active,
    "email": email,
    "first_name": firstName,
    "last_name": lastName,
    "ticket_available": ticketAvailable,
    "offer_available": offerAvailable,
    "distance": distance,
  };
}
