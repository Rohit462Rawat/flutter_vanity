import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_demo/Screens/LoginScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Screens/HomeScreen.dart';

void main() {

  runApp(new MaterialApp(
    theme: ThemeData(accentColor: Colors.lightBlue,
    primarySwatch: Colors.pink
        ,primaryColor:  Colors.pink
    ),
    home: new SplashScreen(),
    routes: <String, WidgetBuilder>{
      '/LoginScreen': (BuildContext context) => new LoginScreen(),
       '/HomeScreen': (BuildContext context) => new HomeScreen(),
    },
  ));
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences prefs;
  startTime() async {
    prefs = await SharedPreferences.getInstance();
    var _duration = new Duration(seconds: 2);
     print(prefs.getString('token'));
    if(prefs.getString('token')!=""&&prefs.getString('token')!=null){
      return new Timer(_duration, navigationHomePage);
    }else{
      return new Timer(_duration, navigationLoginPage);
    }

  }


  void navigationLoginPage() {
    Navigator.of(context).pushReplacementNamed('/LoginScreen');
  }

  void navigationHomePage() {
    Navigator.of(context).pushReplacementNamed('/HomeScreen');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset('assets/images/splash.png',
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
            /* alignment: Alignment.center,*/),
          Center(child:
          Image.asset('assets/images/vanity_logo.png',
            //fit: BoxFit.cover,
            height: 250,
            width: 250,
            )),


        ],
      ),
    );
  }
}