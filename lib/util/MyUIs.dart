import 'package:flutter/material.dart';

  Widget textFieldBackgroundUI(icon,textField){

    return Container(
        height: 45,
        margin: const EdgeInsets.only(left: 0, top: 15, right: 0),
    // padding: const EdgeInsets.all(3.0),
    decoration: new BoxDecoration(
      color: Colors.white,
    border: new Border.all(color: Colors.grey),
    borderRadius: const BorderRadius.all(
    const Radius.circular(30.0),
    ),
    ),
      child:  Row(
        children: <Widget>[
          Container(
              margin: const EdgeInsets.only(
                  left: 12, top: 10, right: 10, bottom: 10),
              child:  icon),
           Container(
            child:  Flexible(
              child: textField,
            ), //flexible
          ), //container
        ], //widget
      ),);
  }

Widget textFieldBackgroundUI2(text,textField){

  return Container(
    height: 55,
    margin: const EdgeInsets.only(left: 0, top: 15, right: 0),
    // padding: const EdgeInsets.all(3.0),

    child:  Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(left: 0, right: 0,bottom: 15),
            child:Align(alignment:Alignment.topLeft,child: text,)
          //flexible
        ),
        Container(
          child:  Flexible(
            child: textField,
          ), //flexible
        ), //container
      ], //widget
    ),);
}

Widget textFieldBackgroundUI1(icon,textField){

  return Container(
    height: 45,
    margin: const EdgeInsets.only(left: 0, top: 15, right: 0),
    // padding: const EdgeInsets.all(3.0),
    decoration: new BoxDecoration(
      border: new Border.all(color: Colors.pink),
      borderRadius: const BorderRadius.all(
        const Radius.circular(30.0),
      ),
    ),
    child:  Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            margin: const EdgeInsets.only(
                left: 12, top: 10, right: 10, bottom: 10),
            child:  icon),
        Container(
          child:  Flexible(
            child: textField,
          ), //flexible
        ), //container
      ], //widget
    ),);


}

Widget toolbarLayout(name){
  return  AppBar(
    centerTitle: true,
    title: Text(name),
    backgroundColor: Colors.pink,
  );
}


Widget drawerItem(name){
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.fromLTRB(15, 10, 0, 0),
      child: Row(
        children: <Widget>[
          Icon(Icons.home),
          SizedBox(
            width: 20,
          ),
          Text(name, style: TextStyle(fontSize: 15,),)
        ],
      ),
    );
}

