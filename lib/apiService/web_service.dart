import 'dart:convert';

import 'package:flutter_demo/model/EventListRequest.dart';
import 'package:flutter_demo/model/LoginResponse_model.dart';
import 'package:flutter_demo/model/PartnerEventList_model.dart';
import 'package:flutter_demo/model/login_model.dart';
import 'package:flutter_demo/model/modelOfferList/ModelOfferListRequest.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:async';
import 'dart:io';

import 'package:progress_dialog/progress_dialog.dart';

String BASE_URL = 'https://app.vanitypass.com/api';

/*
Future<List<Post>> getAllPosts() async {
  final response = await http.get(url);
  print(response.body);
  return allPostsFromJson(response.body);
}
*/

/*Future<Post> getPost() async{
  final response = await http.get('$url/1');
  return postFromJson(response.body);
}*/

Future<LoginResponse> createLoginRequestApi(Login login) async {
  final response = await http.post('$BASE_URL/auth/login',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY
      },
      body: loginToJson(login)
  );
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    return LoginResponse.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

/*Future<http.Response> createLoginRequestApi(Login login) async{
  final response = await http.post('$url/auth/login',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : 'JDJ5JDEwJEJjd3NaamtFMWQzZUk1QzAwVUE1T3VlZWViWndFb00uQ1RSc3RodVJsQXFzWGtENGNhM01H'
      },
      body: loginToJson(login)
  );
  return response;
}*/



  Future<PartnerEventList> getPartnerEventList() async {
  var BASE_URL="https://admin.yourwuber.com/api";
  final response = await http.get('$BASE_URL/auth/services',
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'XAPIKEY' : MyConstants.XAPIKEY,
    },
  );

  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    print(response.body.toString());
    return PartnerEventList.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<http.Response> getPartnerEventList1(String apptoken,EventListRequest request) async {
  var BASE_URL="https://app.vanitypass.com/api";
  final response = await http.post('$BASE_URL/model/event/list',
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'XAPIKEY' : MyConstants.XAPIKEY,
      'APPTOKEN' : apptoken
    },
      body: eventListRequestToJson(request)
  );
  print(response);
    return response;
}


Future<http.Response> createLoginRequestApi1(Login login) async{
  final response = await http.post('$BASE_URL/auth/login',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY
      },
      body: loginToJson(login)
  );
  return response;
}


Future<http.Response> getModelOfferList(String apptoken,ModelOfferListRequest request) async {
  //var BASE_URL="https://app.vanitypass.com/api";
  final response = await http.post('$BASE_URL/model/offer/list',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY,
        'APPTOKEN' : apptoken
      },
      body: modelOfferListRequestToJson(request)
  );
 // print(response);
  return response;
}


Future<http.Response> getModelTimeAvailabilityList(String apptoken,var offer_id) async {
  //var BASE_URL="https://app.vanitypass.com/api";
  final response = await http.get('$BASE_URL/model/offer/availabilityList/$offer_id',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY,
        'APPTOKEN' : apptoken
      },
  );
  // print(response);
  return response;
}


Future<http.Response> modelChangePasswordApi(request,apptoken) async{
  final response = await http.post('$BASE_URL/model/profile/changepass',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY,
        'APPTOKEN' : apptoken
      },
      body: request
  );
  return response;
}

Future<http.Response> partnerChangePasswordApi(request,apptoken) async{
  final response = await http.post('$BASE_URL/partner/profile/changepass',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY,
        'APPTOKEN' : apptoken
      },
      body: request
  );
  return response;
}

Future<http.Response> getReserverTicketsApi(apptoken) async{
  final response = await http.get('$BASE_URL/model/profile/getReserverTickets',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY,
        'APPTOKEN' : apptoken
      },
  );
  return response;
}

Future<http.Response> getModelProfileApi(apptoken) async{
  final response = await http.get('$BASE_URL/model/profile/get',
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'XAPIKEY' : MyConstants.XAPIKEY,
      'APPTOKEN' : apptoken
    },
  );
  return response;
}

Future<http.Response> updateModelProfileApi(jsonReq,apptoken) async{
  final response = await http.post('$BASE_URL/model/profile/edit',
    headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      'XAPIKEY' : MyConstants.XAPIKEY,
      'APPTOKEN' : apptoken
    },
    body: jsonReq
  );
  return response;
}
Future<http.Response> uploadFile(_image,apptoken) async {
    String apiUrl = '$BASE_URL/model/profile/image';
    final length = await _image.length();
    final request = new http.MultipartRequest('POST', Uri.parse(apiUrl))..
    files.add(new http.MultipartFile('file', _image.openRead(), length));
    request.headers['XAPIKEY'] = MyConstants.XAPIKEY;
    request.headers['APPTOKEN'] = apptoken;

    http.Response response = await http.Response.fromStream(await request.send());
    print("Result: ${response.body}");
    return response;

}


/*
Future<http.Response> uploadFile1(_image,apptoken) async {
  var uri = Uri.parse("http://pub.dartlang.org/packages/create");
  var request = new http.MultipartRequest("POST", uri);
  request.fields['user'] = 'nweiz@google.com';
  request.files.add(new http.MultipartFile.fromPath(
      'package',
      'build/package.tar.gz',
      contentType: new MediaType('application', 'x-tar'));
      var response = await request.send();
  if (response.statusCode == 200) print('Uploaded!');

}
*/




//Future<Post> createPost(Post post) async{
//  final response = await http.post('$url',
//      headers: {
//        HttpHeaders.contentTypeHeader: 'application/json'
//      },
//      body: postToJson(post)
//  );
//
//  return postFromJson(response.body);
//}
