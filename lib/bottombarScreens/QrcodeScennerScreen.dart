import 'dart:async';

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';


class QrcodeScennerScreen extends StatefulWidget {
  @override
  _QrcodeScennerScreenState createState() => _QrcodeScennerScreenState();
}

class _QrcodeScennerScreenState extends State<QrcodeScennerScreen> {
  String result = "Hey there !";


  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body: Align(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
                result,
                style: new TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
              ),


          GestureDetector(
            onTap:() {
              _launchURL(result);
            },
            child: Container(
                height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                // padding: const EdgeInsets.all(3.0),

                decoration: new BoxDecoration(
                  color: Colors.pink,
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(30.0),
                  ),
                ),
                child: Center(
                    child: Text("Go To Website",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 18),
                        textDirection: TextDirection.ltr))),
          ),
        ],
      ),
    ),
    floatingActionButton: FloatingActionButton.extended(
    icon: Icon(Icons.camera_alt),
    label: Text("Scan"),
    onPressed: _scanQR,
    ),
    floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,)
    ;
  }


  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult;
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
        });
      } else {
        setState(() {
          result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        result = "Unknown Error $ex";
      });
    }
  }

  _launchURL(url)  {
  //  var url = 'https://flutter.io';
   // if (canLaunch(url)) {
       launch(url);
    /*} else {
      throw 'Could not launch $url';
    }*/
  }

}
