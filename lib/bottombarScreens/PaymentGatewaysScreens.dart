import 'package:flutter/material.dart';
import 'package:flutter_demo/util/MyUtils.dart';
import 'package:flutter_braintree/flutter_braintree.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'dart:io' show Platform;


class PaymentGatewaysScreens extends StatefulWidget {
  @override
  _PaymentGatewaysScreensState createState() => _PaymentGatewaysScreensState();
}



class _PaymentGatewaysScreensState extends State<PaymentGatewaysScreens> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
      GestureDetector(
      onTap: (){
    startPaymentRajorPay();
    },
      child: Container(
          height: 50,
          width: double.infinity,
          margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
          // padding: const EdgeInsets.all(3.0),

          decoration: new BoxDecoration(
            color: Colors.pink,
            borderRadius: const BorderRadius.all(
              const Radius.circular(30.0),
            ),
          ),
          child: Center(
              child: Text("Pay Now RajorPay",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 18),
                  textDirection: TextDirection.ltr))),
    ),
          GestureDetector(
            onTap: (){
              /*if (Platform.isAndroid) {
                startPaymentPayPal();
              } else if (Platform.isIOS) {
              */  braintreeNativeDropIn();
              //}

            },
            child: Container(
                height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                // padding: const EdgeInsets.all(3.0),

                decoration: new BoxDecoration(
                  color: Colors.pink,
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(30.0),
                  ),
                ),
                child: Center(
                    child: Text("Pay Now Braintree(Drop In)",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 18),
                        textDirection: TextDirection.ltr))),
          ),

          GestureDetector(
            onTap: (){
              startPaymentPayPal();
            },
            child: Container(
                height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                // padding: const EdgeInsets.all(3.0),

                decoration: new BoxDecoration(
                  color: Colors.pink,
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(30.0),
                  ),
                ),
                child: Center(
                    child: Text("Pay Now Braintree(PayPal)",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 18),
                        textDirection: TextDirection.ltr))),
          ),
          GestureDetector(
            onTap: (){
              startPaymentPayPalCreditCard();
            },
            child: Container(
                height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(left: 20, top: 20, right: 20),
                // padding: const EdgeInsets.all(3.0),

                decoration: new BoxDecoration(
                  color: Colors.pink,
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(30.0),
                  ),
                ),
                child: Center(
                    child: Text("Pay Now Braintree(Credit Card)",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 18),
                        textDirection: TextDirection.ltr))),
          ),


        ],

      ),
    );

  }


  startPaymentRajorPay() async {
    var _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);


    var options = {
      'key': 'rzp_live_ILgsfZCZoFIKMb',
      'amount': 100,
      'name': 'Acme Corp.',
      'description': 'Fine T-Shirt',
      'prefill': {
        'contact': '8888888888',
        'email': 'test@razorpay.com'
      }
    };

    _razorpay.open(options);

    /*Map<String, dynamic> options = new Map();
    options.putIfAbsent("name", () => "Razorpay T-Shirt");
    options.putIfAbsent("image", () => "https://www.73lines.com/web/image/12427");
    options.putIfAbsent("description", () => "This is a real transaction");
    options.putIfAbsent("amount", () => "100");
    options.putIfAbsent("email", () => "test@testing.com");
    options.putIfAbsent("contact", () => "9988776655");
    //Must be a valid HTML color.
    options.putIfAbsent("theme", () => "#FF0000");
    //Notes -- OPTIONAL
    Map<String, String> notes = new Map();
    notes.putIfAbsent('key', () => "value");
    notes.putIfAbsent('randomInfo', () => "haha");
    options.putIfAbsent("notes", () => notes);
    options.putIfAbsent("api_key", () => "rzp_live_ILgsfZCZoFIKMb");
    Map<dynamic,dynamic> paymentResponse = new Map();
    paymentResponse = await Razorpay.showPaymentForm(options);
    print("response $paymentResponse");
    if(paymentResponse["code"]==1){
      MyUtils.showtoast("Payment success");
    }else
      {
        MyUtils.showtoast("Payment failed");
      }*/

  }


  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    print(response.orderId);
    MyUtils.showtoast("Payment success");
    // Do something when payment succeeds
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    print(response.code);
    print(response.message);
    MyUtils.showtoast("Payment failed"+response.message);
    // Do something when payment fails
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    print(response);

    // Do something when an external wallet was selected
    MyUtils.showtoast("Payment failed");
  }

  startPaymentPayPal() async {
    final request = BraintreePayPalRequest(amount: '500.00');

   /* final request = BraintreePayPalRequest(
    billingAgreementDescription: 'I hearby agree that flutter_braintree is great.',
    );*/

    BraintreePaymentMethodNonce result = await Braintree.requestPaypalNonce(
    'sandbox_bn2p8q5z_73rbr893q5m63n3s',
    request,
    );
    if (result != null) {
    print('Nonce: ${result.nonce}');
    print('Nonce: ${result.toString()}');

    MyUtils.showtoast("success from paypal."+result.toString());
    } else {
      MyUtils.showtoast("PayPal flow was canceled.");
    print('PayPal flow was canceled.');
    }




  }

  startPaymentPayPalCreditCard() async{
    final request = BraintreeCreditCardRequest(
      cardNumber: '4111111111111111',
      expirationMonth: '12',
      expirationYear: '2021',
    );

    BraintreePaymentMethodNonce result = await Braintree.tokenizeCreditCard(
      'sandbox_bn2p8q5z_73rbr893q5m63n3s',
      request,
    );

    if (result != null) {
      print('Nonce: ${result.nonce}');

      MyUtils.showtoast("success from Credit Card."+result.toString());
    } else {
      MyUtils.showtoast("PayPal flow was canceled.");
      print('PayPal flow was canceled.');
    }
  }


  braintreeNativeDropIn() async{
    final request = BraintreeDropInRequest(
      clientToken: 'sandbox_bn2p8q5z_73rbr893q5m63n3s',
      collectDeviceData: true,
      googlePaymentRequest: BraintreeGooglePaymentRequest(
        totalPrice: '800',
        currencyCode: 'USD',
        billingAddressRequired: false,
      ),
      paypalRequest: BraintreePayPalRequest(
        amount: '800',
        displayName: 'Example company',
      ),
    );

    BraintreeDropInResult result = await BraintreeDropIn.start(request);

    if (result != null) {
      print('Nonce: ${result.paymentMethodNonce.nonce}');

      MyUtils.showtoast("success from braintree drop in."+result.paymentMethodNonce.nonce);
    } else {
      MyUtils.showtoast("PayPal flow was canceled.");
      print('PayPal flow was canceled.');
    }

   /* String clientNonce = "GET YOUR CLIENT NONCE FROM THE YOUR SERVER";

    BraintreePayment braintreePayment = new BraintreePayment();
    var data = await braintreePayment.showDropIn(
        nonce: clientNonce, amount: "29.0", enableGooglePay: true);


    print("Response of the payment $data");*/

  }


}