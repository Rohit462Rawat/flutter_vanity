import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_demo/Screens/OfferListScreen.dart';
import 'package:flutter_demo/Screens/SubPage.dart';
import 'package:flutter_demo/apiService/web_service.dart';
import 'package:flutter_demo/model/EventListRequest.dart';
import 'package:flutter_demo/model/PartnerEventList_model.dart';
import 'package:flutter_demo/util/MyConstants.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:location/location.dart';

class VanityMainScreen extends StatefulWidget {
  @override
  _VanityMainScreenState createState() => _VanityMainScreenState();
}

class _VanityMainScreenState extends State<VanityMainScreen> {
  //for location current
  //Location location = Location();
 static Map<String, double> currentLocation;



  SharedPreferences prefs;
  Completer<GoogleMapController> _controller = Completer();

    LatLng _latlng1 =LatLng(28.6221846, 77.3830408);
  //static LatLng _latlng1 =  LatLng(28.6221846, 77.3830408);
  //static LatLng _latlng1 =  LatLng(0.0, 0.0);
  final Set<Marker> _markers = {};
  LatLng _lastMapPosition;

  String type = "list";
  List<Datum> eventList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    width: 150,
                    child: RaisedButton(shape: RoundedRectangleBorder(
                        borderRadius:  BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(0),bottomLeft:Radius.circular(20.0),bottomRight: Radius.circular(0) )),
                        onPressed: listView_onPressed,
                        child: Text(
                          "List View",
                          style: TextStyle(
                              color:
                                  type == "list" ? Colors.white : Colors.black),
                        ),
                        color: type == "list" ? Colors.pink : Colors.white,
                        disabledColor:
                            type == "list" ? Colors.pink : Colors.white)),
                Container(
                  width: 150,
                  child: RaisedButton(shape: RoundedRectangleBorder(
                      borderRadius:  BorderRadius.only(
                          topLeft: Radius.circular(0),
                          topRight: Radius.circular(20.0),bottomLeft:Radius.circular(0),bottomRight: Radius.circular(20.0) )),
                      onPressed: mapView_onPressed,
                      child: Text("Map View",
                          style: TextStyle(
                              color:
                                  type == "map" ? Colors.white : Colors.black)),
                      color: type == "map" ? Colors.pink : Colors.white,
                      disabledColor:
                          type == "map" ? Colors.pink : Colors.white),
                )
              ],
            ),
            height: 50.0,
          ),
          changeListMapView()
        ],
      )),
    );
  }

  changeListMapView() {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 60, 0, 0),
        child: type == "list"
            ? bodyUI_WithApi_listView()
            :mapViewUI());
  }

  listView_onPressed() {
    type = "list";
    setState(() {});
  }

  mapView_onPressed() {
    type = "map";
    setState(() {});
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }
  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);

  }

  void _onAddMarker(List<Datum> eventList) {
   /* setState(() {
      _markers.add(Marker(//onTap:_onMarkerClick() ,
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(_latlng1.toString()),
        position: _latlng1,
        infoWindow: InfoWindow(
          //onTap: _onMarkerClick(),
          title: 'Really cool place',
          snippet: '5 Star Rating',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });*/
  for (int i=0;i<eventList.length;i++){
    Marker resultMarker = Marker(
      //icon: BitmapDescriptor.fromAsset('assets/images/dot.png'),
      consumeTapEvents: false,

      markerId: MarkerId(eventList[i].name),
      infoWindow: InfoWindow(
        onTap: () {
          _onMarkerClick(eventList[i]);
        },
          title: "${eventList[i].name}",
         /* snippet: "${eventList[i].name}"*/),
      position: LatLng(double.parse(eventList[i].latitude),
        double.parse(eventList[i].longitude)),
    );
    _markers.add(resultMarker);
  }
  ;

// Add it to Set

  }

 void onMarkerTapped(Marker marker) {

   Navigator.of(context).push(new MaterialPageRoute(
       builder: (BuildContext context) => SubPage()));
 }

 _onMarkerClick(Datum data){
    print(data.name.toString());
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => OfferListScreen(offerDataList:data,distance:data.distance,latitude:data.latitude,longitude:data.longitude)));

 }

  mapViewUI(){
    return  GoogleMap(
      //onTap: _onAddMarker(),
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _latlng1,
        zoom: 11.0,
      ),
      mapType: MapType.normal,
      markers: _markers,
      onCameraMove: _onCameraMove,
      myLocationButtonEnabled: true,
      myLocationEnabled: true,

    );
  }

  @override
  void initState() {
    super.initState();
    loadsharedPrefrenceWithApi();
    var location = new Location();
     var locationData= location.getLocation().toString();


    // for current location

   // _latlng1= LatLng(currentLocation.latitude, currentLocation.longitude);
    location.onLocationChanged().listen((LocationData currentLocation ) {
      setState(() {
        print(currentLocation.latitude.toString()+currentLocation.latitude.toString());
       _latlng1= LatLng(currentLocation.latitude, currentLocation.longitude);
        _lastMapPosition=_latlng1;
        MyConstants.currentLat=currentLocation.latitude;
        MyConstants.currentLong=currentLocation.longitude;

        setMarker_OnCurrentLocation();
      });
    });
  }

  setMarker_OnCurrentLocation(){
    Marker resultMarker = Marker(
      markerId: MarkerId("You"),
      infoWindow: InfoWindow(
          title: "You",
          snippet: "Harimohan"),
      position: LatLng(_latlng1.latitude,_latlng1.longitude),
    );
    _markers.add(resultMarker);
  }

  loadsharedPrefrenceWithApi() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      callEventListApi(prefs.getString('token'));
    });
  }

  callEventListApi(String apptoken) {
    /* ProgressDialog progressDialog =
    new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage('Please wait…');
    progressDialog.show();*/
    EventListRequest Req = EventListRequest(
        latitude: "28.6221846",
        longitude: "77.3830408",
        radius: '50',
        unitType: 'mi',
        timezone: 'Asia/Calcutta');

    getPartnerEventList1(apptoken, Req).then((response) {
      if (response.statusCode == 200) {
        print(response.body);
        // progressDialog.update(message: 'Few more seconds…');
        // progressDialog.hide();

        this.setState(() {
          final jsonResponse = json.decode(response.body);
          PartnerEventList partnerEventList =
              PartnerEventList.fromJson(jsonResponse);
          /*     for(int i=0;i<partnerEventList.data.length-1;i++) {
            eventList.add(partnerEventList.data[i]);
          }*/
          eventList = partnerEventList.data;
          _onAddMarker(eventList);
        });
      } else {
        print(response.statusCode);
      }
    }).catchError((error) {
      print('error : $error');
    });
  }

  /* fetchData() async {
    var res = await http.get(url);
    var decodedJson = jsonDecode(res.body);
    pokeHub = PokeHub.fromJson(decodedJson);
    print(pokeHub.toJson());
    setState(() {});


    var BASE_URL="https://admin.yourwuber.com/api";
    final response = await http.get('$BASE_URL/auth/services',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        'XAPIKEY' : MyConstants.XAPIKEY,
      },
    );

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      print(response.body.toString());
      return PartnerEventList.fromJson(json.decode(response.body));
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }*/

/*
  Widget bodyUI_withApi_FutureBuilder() {
    return FutureBuilder<PartnerEventList>(
      future: getPartnerEventList(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
              itemCount: snapshot.data.data.length,
              itemBuilder: (context, index) {
                Datum datum = snapshot.data.data[index];
                return Card(
                  elevation: 3,
                  margin: EdgeInsets.all(10),
                  child: Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.all(10),
                              child: CircleAvatar(
                                radius: 40.0,
                                backgroundImage: NetworkImage(
                                    MyConstants.SERVICE_IMAGE + datum.image),
                                backgroundColor: Colors.transparent,
                                */
/*child: FadeInImage.assetNetwork(
                                  placeholder: 'assets/images/loading.gif',
                                  image: MyConstants.SERVICE_IMAGE,
                                  width: 60,
                                  height: 60,
                                ),*/ /*

                              )
                            */
/*Image.network(
                                MyConstants.SERVICE_IMAGE+datum.image,
                                width: 60,
                                height: 60,
                                fit:BoxFit.fill )*/ /*

                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.all(5),
                              padding: EdgeInsets.all(5),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(datum.name,
                                          style: TextStyle(fontSize: 15))),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(datum.description,
                                      style: TextStyle(fontSize: 16)),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),
                );
              });
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner.
        return Center(child: CircularProgressIndicator());
      },
    );
  }
*/

  Widget bodyUI_WithApi_listView() {
    return eventList == null
        ? Center(
            child: CircularProgressIndicator(),
          )
        : ListView.builder(
            itemCount: eventList.length,
            itemBuilder: (context, index) {
              //Datum datum = eventList.data[index];
              return InkWell(
                onTap:(){ onCardTap(
                    eventList[index]);},
                child: Card(
                  elevation: 3,
                  margin: EdgeInsets.all(10),
                  child: Container(
                      child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.all(10),
                          child: CircleAvatar(
                            //child: Image.asset('assets/images/app_icon.png'),
                            radius: 40.0,
                            backgroundImage: NetworkImage(
                                MyConstants.EVENT_IMAGE +
                                    eventList[index].organiserLogo.toString()),
                            backgroundColor: Colors.grey,
                            /*child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/loading.gif',
                                    image: MyConstants.SERVICE_IMAGE,
                                    width: 60,
                                    height: 60,
                                  ),*/
                          )
                          /*Image.network(
                                  MyConstants.SERVICE_IMAGE+datum.image,
                                  width: 60,
                                  height: 60,
                                  fit:BoxFit.fill )*/
                          ),
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(eventList[index].name.toUpperCase(),
                                      style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold))),
                              SizedBox(
                                height: 5,
                              ),

                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(eventList[index].address,
                                    style: TextStyle(fontSize: 13)),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(eventList[index].distance+" KM",
                                    style: TextStyle(fontSize: 13)),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
                ),
              );
            });
  }

// By default, show a loading spinne;

  onCardTap(Datum data){
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => OfferListScreen(offerDataList:data,distance:data.distance,latitude:data.latitude,longitude:data.longitude)));
  }

}
