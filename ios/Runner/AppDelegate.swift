import UIKit
import Flutter
import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyBz74QOmwoft4e7wh_KLFpE7cZztBQAtD0")
    GeneratedPluginRegistrant.register(with: self)
    BTAppSwitch.setReturnURLScheme("com.example.flutter_demo.braintree")
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}